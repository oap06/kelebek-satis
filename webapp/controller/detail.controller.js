sap.ui.define([
	"./BaseController",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageBox",
	"sap/ui/model/json/JSONModel"
], function(BaseController, Filter, FilterOperator, MessageBox, JSONModel) {
	"use strict";

	return BaseController.extend("zcrm.zcrm_sales_order_operation.controller.detail", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf zcrm.zcrm_sales_order_operation.view.detail
		 */
		onInit: function() {
			BaseController.prototype.onInit.apply(this, arguments);
			this.getView().addStyleClass("sapUiSizeCompact");
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("detail").attachPatternMatched(this._onObjectMatched, this);
		},
		_onObjectMatched: function(oEvent) {
			var oGuid = oEvent.getParameter("arguments").guid;
			var oSet = "/headerSet(Guid=guid'" + oGuid + "')";
			var oModel = this.getView().getModel();
			var oModelJsonList = new sap.ui.model.json.JSONModel();
			var oBusyDialog = new sap.m.BusyDialog();
			var oView = this.getView();

			oBusyDialog.open();

			// Header seti aşağıdaki navigationlar ile beraber okuyoruz. Bunların hepsine ihtiyacımız olacak.
			oModel.read(oSet, {
				urlParameters: {
					"$expand": "ToTexts,ToCumulate,ToCustomerExt,ToDates,ToItems,ToOrgman,ToPartners,ToPricing,ToStatus,ToTahsilat,ToMessages,ToConfig,ToSales,ToDocflow,ToCondEasyEntries"
				},
				success: function(data, response) {
					oModelJsonList.setData(data);
					oView.setModel(oModelJsonList, "oModel");
					oBusyDialog.close();
				},
				error: function(oError) {}
			});
		},
		onShowPDF: function() {
			var oBusyDialog = new sap.m.BusyDialog();
			var model = this.getView().getModel("oModel");
			var object_id = model.getProperty("/ObjectId");
			var processType = model.getProperty("/ProcessType");
			if (processType !== 'ZA01') {

				var model1 = this.getView().getModel();
				var urlSet = "/showPDFSet(object_id='" + object_id + "',Form_Type='S')";
				// var urlSet = "/showPDFSet(object_id='" + object_id + "')";

				oBusyDialog.open();
				model1.read(urlSet, {
					success: function(oData, response) {
						// var oDataKey = "/showPDFSet('" + object_id + "')/url";
						var oDataKey = "/showPDFSet(Form_Type='S',object_id='" + object_id + "')/url";
						var pdfUrl = model1.getData(oDataKey);
						oBusyDialog.close();
						window.open(pdfUrl);

					},
					error: function(oError) {
						oBusyDialog.close();
					}

				});

			}

		},

		toBack: function(oEvent) {
			// sap.ui.core.UIComponent.getRouterFor(this).navTo("Targetmain", true);
			this.getView().getModel("oModel").setData(null);
			this.getView().getModel("oModel").refresh();
			history.back();
		},
		onEdit: function() {
			var oGuid = this.getView().getModel("oModel").oData.Guid;
			var oOrderType = this.getView().getModel("oModel").oData.ProcessType;

			// setleri okumak uzun sürüyor. bu yüzden burda okuduğum modeli düzenleme ekranında tekrar okumamamk için global bir
			// modele atıyorum. edit ekranında alacağım bunu
			sap.ui.getCore().setModel(this.getView().getModel("oModel"), "oModelGlobal");

			sap.ui.core.UIComponent.getRouterFor(this).navTo("CreateEdit", {
				guid: oGuid,
				orderType: oOrderType
			});

		},
		formatterToplam: function(o1, o2) {

			var newNum = Number(o1) + Number(o2);
			return newNum.toFixed(2) || 0;
		},

		formatterBirlestir: function(o1, o2, o3) {
			return String(o1 || "") + String(o2 || "") + String(o3 || "");
		},

		formatterBirlestirYadaSifir: function(o1, o2, o3) {
			return String(o1 || "") + String(o2 || "") + String(o3 || "") || 0;
		},

		onDocDetail: function(oEvent) {
			debugger;
			var oNo = oEvent.getSource().getBindingContext("oModel").getProperty("Guid");
			var oProcType = oEvent.getSource().getBindingContext("oModel").getProperty("ProcessType");
			if (oProcType === "ZS05") {
				var oCrossData = new JSONModel();
				oCrossData.setProperty("/sendToTeklif", {
					mode: "display",
					Guid: oNo
				});
				sap.ui.getCore().setModel(oCrossData, "crossData");

				var xnavservice = sap.ushell && sap.ushell.Container && sap.ushell.Container.getService && sap.ushell.Container.getService(
					"CrossApplicationNavigation");

				xnavservice.toExternal({
					target: {
						semanticObject: "ZCRM_QUOT",
						action: "display"
					}
				});
			} else {
				// var oNo = oEvent.getSource().getBindingContext("oModel").getProperty("HeaderGuid");
				var oNo = oEvent.getSource().getBindingContext("oModel").getProperty("Guid");
				sap.ui.core.UIComponent.getRouterFor(this).navTo("detail", {
					guid: oNo
				});
			}
		},
		onVirtualPayment: function() {

			window.open("https://portal.buluttahsilat.com/");

		},
		setEnabledforPrint: function(odemeTuru, makbuzNo, kasayaGonder) {
			if (odemeTuru === "04" && kasayaGonder === true) { // && makbuzNo !== "" && makbuzNo !== undefined && makbuzNo !== null
				return true;
			} else {
				return false;
			}

		},
		onMakbuzCikti: function(oEvent) {
			var that = this;
			var sPath = oEvent.getSource().getParent().getBindingContext("oModel").sPath;
			var n = this.getView().getModel("oModel").getProperty(sPath);

			var oBusyDialog = new sap.m.BusyDialog();

			var model = this.getView().getModel();
			var path = "/showPdfMakbuzSet(Hguid=guid'" + n.ParentId + "',Tguid=guid'" + n.RecordId + "')";

			oBusyDialog.open();
			model.read(path, {
				success: function(oData, response) {

					oBusyDialog.close();
					if (response.data.Makbuz !== "" && response.data.Makbuz !== null && response.data.Makbuz !== undefined) {
						var oInd = sPath.slice(-1);
						var oModel = that.getView().byId("TahsilatTabId").getModel("oModel");
						var oTab = oModel.getProperty("/ToTahsilat/results");
						oTab[oInd].Zzfld0000at = response.data.Makbuz;
						oModel.setProperty("/ToTahsilat/results", oTab);

					}
					window.open(response.data.Url);

				},
				error: function(oError) {
					oBusyDialog.close();
				}

			});

		}

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf zcrm.zcrm_sales_order_operation.view.detail
		 */
		// onBeforeRendering: function () {
		// },

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf zcrm.zcrm_sales_order_operation.view.detail
		 */
		// onAfterRendering: function () {
		// },

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf zcrm.zcrm_sales_order_operation.view.detail
		 */
		//	onExit: function() {
		//
		//	}

	});

});