sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"zcrm/zcrm_sales_order_operation/model/models",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"zcrm/zcrm_sales_order_operation/controller/FragmentController"
], function (UIComponent, Device, models, JSONModel, Filter, FilterOperator, FragmentController) {
	"use strict";

	return UIComponent.extend("zcrm.zcrm_sales_order_operation.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function () {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// enable routing
			this.getRouter().initialize();

			// set the device model
			this.setModel(models.createDeviceModel(), "device");
			this.fragments = {};
			this.setModel(new JSONModel({}), "local");
			this.setModel(new JSONModel({}), "search");
			this.setModel(new JSONModel({}), "headersearch");
			this.setModel(new JSONModel({}), "selected");
			this.setModel(new JSONModel({}), "actionResultMessage");
			this.setModel(new JSONModel({}), "oToplamModel");
			this.setModel(new JSONModel({}), "valuestate");
			this.setModel(new JSONModel({}), "oConfig");
			this.setModel(new JSONModel({}), "KampanyaBelirle");
			this.setModel(new JSONModel({}), "MerkeziKontrol");
			this.setModel(new JSONModel({}), "visible");
			
			this.getModel("headersearch").setSizeLimit(9999999);
			this.checkFromOtherApps();
		},
		exit: function () {
			Object.keys(this.fragments).forEach(function destroyFragment(sFragmentName) {
				this.fragments[sFragmentName].oDialog.destroy();
				delete this.fragments[sFragmentName];
			}.bind(this));
		},

		openFragment: function (sFragmentName, oView) {
			if (!this.fragments[sFragmentName]) {
				var oRootView = this.byId(this.getMetadata().getRootView().id);
				var oController = new FragmentController();
				var sPath = [this.getMetadata().getComponentName(), "fragments", sFragmentName].join(".");

				this.fragments[sFragmentName] = {
					oDialog: sap.ui.xmlfragment(sPath, oController),
					oController: oController
				};
				oRootView.addDependent(this.fragments[sFragmentName].oDialog);
				oController.setDialog(this.fragments[sFragmentName].oDialog);
				oController.setView(oView);
			}
			this.fragments[sFragmentName].oDialog.open();
			return this.fragments[sFragmentName].oController;
		},
		checkFromOtherApps: function () {
			var oCrossModel = sap.ui.getCore().getModel("crossData");
			if (!oCrossModel) {
				return;
			}
			var oCrossData = oCrossModel.getProperty("/sendToSiparis");
			sap.ui.getCore().setModel(null, "crossData");
			if (oCrossData) {
				sap.ui.getCore().setModel(new JSONModel(oCrossData), "oModelGlobal");
				if(oCrossData.mode === "display") {
					this.getRouter().navTo("detail", {
						guid: oCrossData.Guid
					}, true);
				} else {
					this.getRouter().navTo("CreateEdit", {
						guid: oCrossData.Guid,
						orderType: oCrossData.ProcessType
					}, true);
				}
			}
		},
	});
});