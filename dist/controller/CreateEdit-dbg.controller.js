sap.ui.define([
	"./BaseController",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageBox",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/BusyIndicator",
	"zcrm/zcrm_sales_order_operation/controls/ExtScanner"
], function(BaseController, Filter, FilterOperator, MessageBox, JSONModel,BusyIndicator,ExtScanner) {
	"use strict";

	return BaseController.extend("zcrm.zcrm_sales_order_operation.controller.CreateEdit", {

		onInit: function() {
			this.oScanner = new ExtScanner({
				settings: true,
				laser: true,
				valueScanned: this.onScanned.bind(this),
				decoderKey: "text",
				decoders: this.getDecoders(),
			  });
			BaseController.prototype.onInit.apply(this, arguments);
			this.getView().addStyleClass("sapUiSizeCompact");
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("CreateEdit").attachPatternMatched(this._onObjectMatched, this);
			oRouter.getRoute("CreateFromPartner").attachPatternMatched(this._onObjectMatched, this);

			// Burada tablodaki checkbox alanlar için tipi extend ediyoruz. Gerekli
			sap.ui.model.SimpleType.extend("sap.ui.model.type.Boolean", {
				formatValue: function(oValue) {
					if (oValue === "X") {
						return true;
					}
					if (oValue === null || oValue === "") {
						return false;
					}
				},
				parseValue: function(oValue) {
					if (oValue === true) {
						return "X";
					} else if (oValue === false) {
						return "";
					}
				},
				validateValue: function(oValue) {
					return oValue;
				}

			});
			//  
			// 	var guid = "00000000-0000-0000-0000-000000000000";
			// 	this.disableElements(guid,"", "ZA14");
		},

		     // Barcode Scanner Begin
			 onScanned: function (oEvent) {
				var that = this;
				var value = oEvent.getParameter("value");
		
				var oFilters = [];
				if (value !== "") {
				  var oFilter = new Filter("Gtin", FilterOperator.EQ, value);
				  oFilters.push(oFilter);
				}
				var oBusyDialog = new sap.m.BusyDialog();
				oBusyDialog.open();
				var oModelProd = this.getView().getModel("PROD");
				oModelProd.read("/productGtinSet", {
				  filters: oFilters,
				  success: function (oData, response) {
					try {
					  var ProductID = oData.results[0].Product;
					  that._setbarcode(ProductID);
					} catch (error) {}
		
					oBusyDialog.close();
					debugger;
				  },
				  error: function (oError) {
					oBusyDialog.close();
					debugger;
				  },
				});
				// this.oMainModel.setProperty('/scannedValue', oEvent.getParameter('value'));
			  },
		
			  onScan: function () {
				this.oScanner.open();
			  },
		
			  getDecoders: function () {
				return [
				  {
					key: "PDF417-UII",
					text: "PDF417-UII",
					decoder: this.parserPDF417UII,
				  },
				  {
					key: "text",
					text: "TEXT",
					decoder: this.parserText,
				  },
				];
			  },
		
			  parserText: function (oResult) {
				var sText = "";
				var iLength = oResult.text.length;
				for (var i = 0; i !== iLength; i++) {
				  if (oResult.text.charCodeAt(i) < 32) {
					sText += " ";
				  } else {
					sText += oResult.text[i];
				  }
				}
				return sText;
			  },
		
			  parserPDF417UII: function (oResult) {
				// we expect that
				// first symbol of UII (S - ASCII = 83) or it just last group
				var sText = oResult.text || "";
				if (oResult.format && oResult.format === 10) {
				  sText = "";
				  var iLength = oResult.text.length;
				  var aChars = [];
				  for (var i = 0; i !== iLength; i++) {
					aChars.push(oResult.text.charCodeAt(i));
				  }
				  var iStart = -1;
				  var iGRCounter = 0;
				  var iGroupUII = -1;
				  var sTemp = "";
				  aChars.forEach(function (code, k) {
					switch (code) {
					  case 30:
						if (iStart === -1) {
						  iStart = k;
						  sTemp = "";
						} else {
						  sText = sTemp;
						  iGRCounter = -1;
						}
						break;
					  case 29:
						iGRCounter += 1;
						break;
					  default:
						if (iGRCounter > 2 && code === 83 && iGRCounter > iGroupUII) {
						  sTemp = "";
						  iGroupUII = iGRCounter;
						}
						if (iGroupUII === iGRCounter) {
						  sTemp += String.fromCharCode(code);
						}
					}
				  });
				  if (sText) {
					sText = sText.slice(1);
				  }
				}
				return sText;
			  },
		_onObjectMatched: function(oEvent) {

			// gelen parametreleri oku
			this.oGuid = oEvent.getParameter("arguments").guid;
			this.oOrderType = oEvent.getParameter("arguments").orderType;
			this.oCustomer = oEvent.getParameter("arguments").Partner;

			var oSet = "/headerSet(Guid=guid'" + this.oGuid + "')";
			var oModel = this.getView().getModel();
			var oModelJsonList = new sap.ui.model.json.JSONModel();
			var oBusyDialog = new sap.m.BusyDialog();
			var oView = this.getView();

			var that = this;
			//	var oUname = sap.ushell.Container.getService("UserInfo").getId();
			var oUname = "dummyName";
			//oBusyDialog.open();

			//	var urlSet = "/userInfoSet(Uname='" + oUname + "',Form_Type='S')";
			var urlSet = "/userInfoSet(Uname='" + oUname + "')";
			oModel.read(urlSet, {
				success: function(oData, response) {
					that.userInfo = oData;
					var oModelJsonListUserModel = new sap.ui.model.json.JSONModel(oData);
					oView.setModel(oModelJsonListUserModel, "oUserInfo");
				},
				error: function(oError) {}
			});

			var FollowupItems = this.getView().getModel("selected").getProperty("/FollowupItems", "X");
			if (FollowupItems) {
				var FollowupModel = sap.ui.getCore().getModel("FollowupModel");
				var data = FollowupModel.oData;
				this.getView().getModel("oConfig").setProperty("/", data.ToConfig);
				if (data.ToConfig && data.ToConfig.results) {
					data.ToConfig.results.forEach(function(line) {
						this.loadLineConfig(line, oModel);
					}.bind(this));
				}
				that.bindElements(data);
				oView.setModel(FollowupModel, "oModel");
				// this.onSave(false);
				that.disableElements(data.Guid, data.ToStatus.Status, that.oOrderType);
				that.bindSearchHelps(data);
				oBusyDialog.close();
			} else {

				// Eğer detail metodundan geliyorsak modeli tekrar okumaya gerek yok ordaki modeli alıyoruz. değilse baştan okuyacağız.
				if (sap.ui.getCore().getModel("oModelGlobal")) {
					oModel = sap.ui.getCore().getModel("oModelGlobal");
					var data = oModel.oData;
					data.Zzfld000069 = data.Zzfld000069 || "0001";
					this.onSevkiyatYeri("", data.Zzfld000069);
					this.getView().getModel("oConfig").setProperty("/", data.ToConfig);
					if (data.ToConfig && data.ToConfig.results) {
						data.ToConfig.results.forEach(function(line) {
							this.loadLineConfig(line, oModel);
						}.bind(this));
					}

					var oCode = "0005"; //ödeme koşulu
					var oExtension = data.ProcessType;
					this.BaseValueHelp("/valueHelpSet", "/oOdeme", {
						Code: oCode,
						Extension: oExtension
					}, undefined, function onSuccess(oData) {
						var key = this.getOwnerModelProperty("headersearch", "/oOdeme/0/Key");
						this.getView().byId("inpu19").setSelectedKey(key);
						data.ToPricing.Pmnttrms = data.ToPricing.Pmnttrms || key;

					}.bind(this));
					var oCode = "0006"; //ödeme biçimi
					var oExtension = data.ProcessType;
					this.BaseValueHelp("/valueHelpSet", "/oOdemeBicim", {
						Code: oCode,
						Extension: oExtension
					}, undefined, function onSuccess(oData) {
						var key = this.getOwnerModelProperty("headersearch", "/oOdemeBicim/0/Key");
						this.getView().byId("inpu20").setSelectedKey(key);
						data.ToPricing.PaymentMethod = data.ToPricing.PaymentMethod || key;

					}.bind(this));
					this.bindElements(data);
					oView.setModel(oModel, "oModel");
					that.disableElements(data.Guid, data.ToStatus.Status, that.oOrderType);
					that.bindSearchHelps(data);
					that.fillRefIdOnEdit();
					oBusyDialog.close();
				} else {
					oModel.read(oSet, {
						urlParameters: {
							"$expand": "ToTexts,ToCumulate,ToCustomerExt,ToDates,ToItems,ToOrgman,ToPartners,ToPricing,ToStatus,ToTahsilat,ToMessages,ToConfig,ToDocflow,ToCondEasyEntries"
						},
						success: function(data, response) {
							data.ProcessType = that.oOrderType;
							data.ToStatus.Status = data.ToStatus.Status || "E0002";
							data.PostingDate = data.PostingDate || new Date();
							data.ToCustomerExt.Zzfld00009x = data.ToCustomerExt.Zzfld00009x || new Date();
							data.ToPricing.PriceDate = data.ToPricing.PriceDate || new Date();
							data.Zzfld000069 = data.Zzfld000069 || "0001";
							this.onSevkiyatYeri("", data.Zzfld000069);
							var oCode = "0005"; //ödeme koşulu
							var oExtension = data.ProcessType;
							this.BaseValueHelp("/valueHelpSet", "/oOdeme", {
								Code: oCode,
								Extension: oExtension
							}, undefined, function onSuccess(oData) {
								var key = this.getOwnerModelProperty("headersearch", "/oOdeme/0/Key");
								this.getView().byId("inpu19").setSelectedKey(key);
								data.ToPricing.Pmnttrms = data.ToPricing.Pmnttrms || key;

							}.bind(this));
							var oCode = "0006"; //ödeme biçimi
							var oExtension = data.ProcessType;
							this.BaseValueHelp("/valueHelpSet", "/oOdemeBicim", {
								Code: oCode,
								Extension: oExtension
							}, undefined, function onSuccess(oData) {
								var key = this.getOwnerModelProperty("headersearch", "/oOdemeBicim/0/Key");
								this.getView().byId("inpu20").setSelectedKey(key);
								data.ToPricing.PaymentMethod = data.ToPricing.PaymentMethod || key;

							}.bind(this));

							that.bindElements(data); // bazı alanlar özellikle deep entityler modelden dolmuyor.bunları elle bind ediyoruz
							oModelJsonList.setData(data);
							oView.setModel(oModelJsonList, "oModel");
							that.disableElements(data.Guid, data.ToStatus.Status, that.oOrderType); // koşullara göre bazı alanlar kapalı gelecek
							that.bindSearchHelps(data); // search help setlerini çağırıyoruz
							oBusyDialog.close();
						}.bind(this),
						error: function(oError) {
							var message = oError.responseText;
							sap.m.MessageBox.show(message, {
								icon: sap.m.MessageBox.Icon.ERROR,
								title: "HATA",
								actions: [sap.m.MessageBox.Action.OK]
							});
						}
					});
				}
			}

			if (this.getView().getModel("oEditModel") !== undefined) {
				this.getView().getModel("oEditModel").setData(null);
			}

			var editModelData = {};

			if (data !== undefined) {

				if ((data.DistributedI1006 !== undefined && data.DistributedI1006 === "X") && data.ToStatus.ActStatus === "E0003") {
					// if ((data.DistributedI1006 !== undefined && data.DistributedI1006 === "X") || data.ToStatus.ActStatus === "E0003") {

					// if (this.getView().getModel("oEditModel") === undefined) {

					// 	// var oEditModel = new JSONModel({
					// 	// 	edit: false,
					// 	// 	tahsilatEdit: true
					// 	// });
					// 	// this.getView().setModel(oEditModel, "oEditModel");
					// } else {
					// 	editModelData.edit = false;
					// 	this.getView().getModel("oEditModel").setData(editModelData);
					// }

					editModelData.edit = false;

					var oHead = oView.getModel("oModel").getProperty("/");

					if (!(oHead.ProcessType === "ZA04" || oHead.ProcessType === "ZS20" || oHead.ProcessType === "ZA01" || oHead.ProcessType ===
							"ZA14" || oHead.ProcessType === "ZA15")) {

						editModelData.tahsilatEdit = false;
						// this.getView().getModel("oEditModel").setData();
						// this.getView().getModel("oEditModel").setData(editModelData);
					} else {
						editModelData.tahsilatEdit = true;
					}

					if (this.getView().getModel("oEditModel") === undefined) {
						var oEditModel = new JSONModel({});
						this.getView().setModel(oEditModel, "oEditModel");
					}

					this.getView().getModel("oEditModel").setData(editModelData);

				}

				// if ((data.DistributedI1006 !== undefined && data.DistributedI1006 === "X") || data.ToStatus.ActStatus === "E0003") {

				// 	if (this.getView().getModel("oEditModel") === undefined) {

				// 		var oEditModel = new JSONModel({
				// 			edit: false,
				// 			tahsilatEdit: true
				// 		});
				// 		this.getView().setModel(oEditModel, "oEditModel");
				// 	} else {
				// 		this.getView().getModel("oEditModel").getData().edit = false;
				// 	}

				// 	var oHead = oView.getModel("oModel").getProperty("/");
				// 	if (!(oHead.ProcessType === "ZA04" || oHead.ProcessType === "ZS20" || oHead.ProcessType === "ZA01" || oHead.ProcessType ===
				// 			"ZA14" || oHead.ProcessType === "ZA15")) {
				// 		this.getView().getModel("oEditModel").getData().tahsilatEdit = false;
				// 	}

				// }

				//skarakas : talep : siparişe dönüşmüş ve erp sistemine akmış belgelerde sepet pasif olacak.
				// if ((data.DistributedI1006 !== undefined && data.DistributedI1006 === "X") || data.ToStatus.ActStatus === "E0003") {
				// 	// this.getView().byId("iconSepet").setEnabled(false);
				// 	this.getView().byId("IconGenel").setEnabled(false);
				// 	this.getView().byId("IconIsleme").setEnabled(false);
				// 	this.getView().byId("IconTermin").setEnabled(false);
				// 	this.getView().byId("IconDeger").setEnabled(false);
				// 	this.getView().byId("IconKamp").setEnabled(false);
				// 	this.getView().byId("IconTahsilat").setEnabled(false);
				// 	this.getView().byId("IconDegisim").setEnabled(false);
				// 	this.getView().byId("IconNotlar").setEnabled(false);
				// 	this.getView().byId("iconSepet").setEnabled(false);

				// } else {
				// 	this.getView().byId("IconGenel").setEnabled(true);
				// 	this.getView().byId("IconIsleme").setEnabled(true);
				// 	this.getView().byId("IconTermin").setEnabled(true);
				// 	this.getView().byId("IconDeger").setEnabled(true);
				// 	this.getView().byId("IconKamp").setEnabled(true);
				// 	this.getView().byId("IconTahsilat").setEnabled(true);
				// 	this.getView().byId("IconDegisim").setEnabled(true);
				// 	this.getView().byId("IconNotlar").setEnabled(true);
				// 	this.getView().byId("iconSepet").setEnabled(true);
				// }

			}
		},

		stateRequired: function(value, isRequired) {
			if (isRequired !== false && !value) {
				return "Error";
			}
			return "None";
		},

		bindSearchHelps: function(oData) {

			// Arama yardımları servisleri. Neyin arama yardımıysa modele onun adını vermeye çalıştım. 
			var oModel = this.getView().getModel();
			var oView = this.getView();

			var oFilters = [];
			var oFilter = {};
			var oBayi = oView.byId("inpu5");
			var oAdres = oView.byId("inpu6");
			var oOdemeKosul = oView.byId("inpu19");
			var oOdemeBicim = oView.byId("inpu20");
			var oFlist = oView.byId("inpu25");
			var oCalisan = oView.byId("inpu14");
			var oDurum = oView.byId("inpu17");
			var oSKosul = oView.byId("inpu8");

			oFilter = new Filter("Code", FilterOperator.EQ, "0002");
			oFilters.push(oFilter);

			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function(oData, response) {
					var oModelJsonList2 = new sap.ui.model.json.JSONModel(oData);

					oModelJsonList2.setSizeLimit(10000);
					oBayi.setModel(oModelJsonList2, "oBayi");
				},
				error: function(oError) {}
			});
			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0003");
			oFilters.push(oFilter);
			if (oData) {
				oFilter = new Filter("Extension", FilterOperator.EQ, oData.Zzfld000084);
				oFilters.push(oFilter);
			}

			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function(oData, response) {
					var oModelJsonList3 = new sap.ui.model.json.JSONModel(oData);
					oAdres.setModel(oModelJsonList3, "oAdres");
				},
				error: function(oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0004");
			oFilters.push(oFilter);
			if (oData) {
				oFilter = new Filter("Guid", FilterOperator.EQ, oData.Guid);
				oFilters.push(oFilter);
			}
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function(oData, response) {
					var oModelJsonList4 = new sap.ui.model.json.JSONModel(oData);
					oCalisan.setModel(oModelJsonList4, "oCalisan");
				},
				error: function(oError) {}
			});
			var oCode = "0005"; //ödeme koşulu
			var oExtension = oData.ProcessType;
			this.BaseValueHelp("/valueHelpSet", "/oOdeme", {
				Code: oCode,
				Extension: oExtension
			});
			oCode = "0006"; //ödeme biçimi
			oExtension = oData.ProcessType;
			this.BaseValueHelp("/valueHelpSet", "/oOdemeBicim", {
				Code: oCode,
				Extension: oExtension
			});

			oCode = "0022"; //ödeme tipi
			// oExtension = oData.ProcessType;
			this.BaseValueHelp("/valueHelpSet", "/OdemeTipi", {
				Code: oCode,
				// Extension: oExtension
			});
			// oFilters = [];
			// oFilter = new Filter("Code", FilterOperator.EQ, "0005");
			// oFilters.push(oFilter);
			// if (oData) {
			// 	oFilter = new Filter("Extension", FilterOperator.EQ, oData.ProcessType);
			// 	oFilters.push(oFilter);
			// }
			// oModel.read("/valueHelpSet", {
			// 	filters: oFilters,
			// 	success: function (oData, response) {
			// 		var oModelJsonList5 = new sap.ui.model.json.JSONModel(oData);
			// 		oOdemeKosul.setModel(oModelJsonList5, "oOdeme");
			// 	},
			// 	error: function (oError) {}
			// });

			// oFilters = [];
			// oFilter = new Filter("Code", FilterOperator.EQ, "0006");
			// oFilters.push(oFilter);
			// if (oData) {
			// 	oFilter = new Filter("Extension", FilterOperator.EQ, oData.ProcessType);
			// 	oFilters.push(oFilter);
			// }
			// oModel.read("/valueHelpSet", {
			// 	filters: oFilters,
			// 	success: function (oData, response) {
			// 		var oModelJsonList6 = new sap.ui.model.json.JSONModel(oData);
			// 		oOdemeBicim.setModel(oModelJsonList6, "oOdemeBicim");
			// 	},
			// 	error: function (oError) {}
			// });

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0007");
			oFilters.push(oFilter);
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function(oData, response) {
					var oModelJsonList7 = new sap.ui.model.json.JSONModel(oData);
					oFlist.setModel(oModelJsonList7, "oFlist");
				},
				error: function(oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0009");
			oFilters.push(oFilter);
			if (oData) {
				oFilter = new Filter("Extension", FilterOperator.EQ, oData.ProcessType);
				oFilters.push(oFilter);
				oFilter = new Filter("Guid", FilterOperator.EQ, oData.Guid);
				oFilters.push(oFilter);
			}
			oDurum.setValue("");
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function(oData, response) {
					var oModelJsonList8 = new sap.ui.model.json.JSONModel(oData);
					oDurum.setModel(oModelJsonList8, "oDurum");
					oDurum.setSelectedKey(oDurum.getSelectedKey());
				},
				error: function(oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0010");
			oFilters.push(oFilter);
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function(oData, response) {
					var oModelJsonList9 = new sap.ui.model.json.JSONModel(oData);
					oSKosul.setModel(oModelJsonList9, "oSKosul");
				},
				error: function(oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0011");
			oFilters.push(oFilter);
			if (oData) {
				oFilter = new Filter("Extension", FilterOperator.EQ, oData.ProcessType);
				oFilters.push(oFilter);
			}
			oFilters.push(oFilter);
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function(oData, response) {
					var oModelJsonList10 = new sap.ui.model.json.JSONModel(oData);
					oView.setModel(oModelJsonList10, "oDepo");
				},
				error: function(oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0015");
			oFilters.push(oFilter);
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function(oData, response) {
					var oModelJsonList11 = new sap.ui.model.json.JSONModel(oData);
					oView.setModel(oModelJsonList11, "oOdemeTur");
				},
				error: function(oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0016");
			oFilters.push(oFilter);
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function(oData, response) {
					var oModelJsonList12 = new sap.ui.model.json.JSONModel(oData);
					oView.setModel(oModelJsonList12, "oTahsTur");
				},
				error: function(oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0017");
			oFilters.push(oFilter);
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function(oData, response) {
					var oModelJsonList13 = new sap.ui.model.json.JSONModel(oData);
					oView.setModel(oModelJsonList13, "oBanka");
				},
				error: function(oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0018");
			oFilters.push(oFilter);
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function(oData, response) {
					var oModelJsonList14 = new sap.ui.model.json.JSONModel(oData);
					oView.setModel(oModelJsonList14, "oTahsDurum");
				},
				error: function(oError) {}
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0024");
			oFilters.push(oFilter);
			oModel.read("/valueHelpSet", {
				filters: oFilters,
				success: function(oData, response) {
					var oModelJsonList17 = new sap.ui.model.json.JSONModel(oData);
					oView.setModel(oModelJsonList17, "oTahsOncelik");
				},
				error: function(oError) {}
			});

			oModel.read("/unitSet", {
				success: function(oData, response) {
					var oModelJsonList15 = new sap.ui.model.json.JSONModel(oData);
					oModelJsonList15.setSizeLimit(1000);
					oView.setModel(oModelJsonList15, "oUnit");
				},
				error: function(oError) {}
			});

			oModel.read("/currencySet", {
				success: function(oData, response) {
					var oModelJsonList16 = new sap.ui.model.json.JSONModel(oData);
					oModelJsonList16.setSizeLimit(500);
					oView.setModel(oModelJsonList16, "oCurr");
				},
				error: function(oError) {}
			});

		},

		disableElements: function(oGuid, oStat, oOrderType) {
			var oView = this.getView();
			var oVisible = oView.getModel("visible");
			this.enableElements();
			switch (oOrderType) {
				case 'ZA01':
					oVisible.setProperty("/iskonto1", true);
					oVisible.setProperty("/iskonto2", false);
					oView.byId("inpu2").setVisible(false);
					oView.byId("inpu201").setVisible(false);
					oView.byId("inpu3").setVisible(false);
					oView.byId("inpu7").setVisible(false);
					// oView.byId("inpu11").setVisible(false);
					oView.byId("inpu15").setVisible(false);
					oView.byId("inpu16").setVisible(false);
					oView.byId("inpu18").setVisible(false);
					oView.byId("inpu22").setVisible(false);
					oView.byId("inpu23").setVisible(true);
					oView.byId("IconKamp").setVisible(false);
					oView.byId("IconTahsilat").setVisible(false);
					oView.byId("IconDegisim").setVisible(false);
					oView.byId("idTahsScroll").setVisible(false);
					oView.byId("inpu25").setVisible(false);
					oView.byId("inpu251").setVisible(false);

					oView.byId("inpu1").setEditable(false);
					oView.byId("inpu20").setEditable(false);
					oView.byId("inpu21").setEditable(false);
					oView.byId("inpu25").setEditable(false);
					oView.byId("inpu251").setEditable(false);
					oView.byId("inpu25lbl").setVisible(false);
					oView.byId("inpu26").setEditable(false);
					oView.byId("inpu27").setEditable(false);
					oView.byId("inpu28").setEditable(false);

					oView.byId("inpu19").setRequired(true);

					oView.byId("inpu8lbl").setRequired(true);

					oView.byId("inpu19").setValue();

					oView.byId("TeshirSecBut").setVisible(false);
					oView.byId("MerkeziKontrolBut").setVisible(true);
					break;
				case 'ZA04':
					oView.byId("IconNotlar").setVisible(true);
					oVisible.setProperty("/iskonto1", true);
					oVisible.setProperty("/iskonto2", false);

					oView.byId("inpu1").setEditable(false);
					oView.byId("inpu2").setVisible(true);
					// oView.byId("inpu7").setVisible(false);
					oView.byId("inpu201").setVisible(true);
					oView.byId("inpu16").setEditable(false);
					oView.byId("inpu19").setEditable(false);
					oView.byId("inpu20").setEditable(false);
					oView.byId("inpu21").setEditable(false);
					oView.byId("inpu26").setEditable(false);
					oView.byId("inpu27").setEditable(false);
					oView.byId("inpu28").setEditable(false);
					oView.byId("inpu29").setEditable(false);
					oView.byId("inpu30").setEditable(false);
					oView.byId("inpu35").setEditable(false);
					oView.byId("inpu36").setEditable(false);
					oView.byId("inpu37").setEditable(false);
					oView.byId("inpu38").setEditable(false);
					oView.byId("inpu39").setEditable(false);
					oView.byId("inpu40").setEditable(false);
					oView.byId("inpu42").setEditable(false);
					oView.byId("inpu43").setEditable(false);

					oView.byId("inpu25").setVisible(true);
					oView.byId("inpu25lbl").setVisible(true);
					oView.byId("inpu251").setVisible(true);

					oView.byId("inpu8lbl").setRequired(true);

					oView.byId("inpu19lbl").setRequired(false);

					oView.byId("TeshirSecBut").setVisible(false);
					oView.byId("MerkeziKontrolBut").setVisible(false);
					break;
				case 'ZA14':
					oVisible.setProperty("/iskonto1", true);
					oVisible.setProperty("/iskonto2", false);
					oView.byId("inpu2").setVisible(false);
					oView.byId("inpu201").setVisible(false);
					oView.byId("inpu25").setVisible(false);
					oView.byId("inpu25lbl").setVisible(false);
					oView.byId("inpu251").setVisible(false);
					oView.byId("inpu5").setVisible(false);
					oView.byId("inpu6").setVisible(false);
					oView.byId("inpu7").setVisible(false);
					oView.byId("inpu15").setVisible(false);
					oView.byId("inpu16").setVisible(false);
					oView.byId("inpu18").setVisible(false);
					oView.byId("inpu22").setVisible(false);
					oView.byId("inpu23").setVisible(false);
					oView.byId("inpu24").setVisible(false);
					oView.byId("IconDeger").setVisible(false);
					oView.byId("IconKamp").setVisible(false);
					oView.byId("IconTahsilat").setVisible(false);
					oView.byId("IconDegisim").setVisible(false);
					oView.byId("idTahsScroll").setVisible(false);

					oView.byId("inpu1").setEditable(false);
					// oView.byId("inpu11").setEditable(false);
					oView.byId("inpu19").setEditable(false);
					oView.byId("inpu20").setEditable(false);

					oView.byId("inpu8lbl").setRequired(true);

					oView.byId("inpu19lbl").setRequired(false);

					oView.byId("TeshirSecBut").setVisible(false);
					oView.byId("MerkeziKontrolBut").setVisible(false);
					break;

				case 'ZS20':
					oVisible.setProperty("/iskonto1", false);
					oVisible.setProperty("/iskonto2", true);

					oView.byId("inpu2").setVisible(true);
					oView.byId("inpu201").setVisible(true);
					oView.byId("inpu7").setVisible(false);
					oView.byId("inpu15").setVisible(false);
					oView.byId("inpu16").setVisible(false);
					oView.byId("inpu22").setVisible(false);
					oView.byId("inpu23").setVisible(false);
					oView.byId("inpu32").setVisible(false);
					oView.byId("inpu33").setVisible(false);
					oView.byId("inpu34").setVisible(false);
					oView.byId("IconDegisim").setVisible(false);

					oView.byId("inpu1").setEditable(false);
					oView.byId("inpu19").setEditable(false);
					oView.byId("inpu20").setEditable(false);
					oView.byId("inpu21").setEditable(false);
					oView.byId("inpu26").setEditable(false);
					oView.byId("inpu27").setEditable(false);
					oView.byId("inpu28").setEditable(false);
					oView.byId("inpu35").setEditable(false);
					oView.byId("inpu36").setEditable(false);
					oView.byId("inpu37").setEditable(false);
					oView.byId("inpu38").setEditable(false);
					oView.byId("inpu39").setEditable(false);
					oView.byId("inpu40").setEditable(false);
					oView.byId("inpu29").setEditable(false);

					oView.byId("inpu25").setVisible(true);
					oView.byId("inpu25lbl").setVisible(true);
					oView.byId("inpu251").setVisible(true);

					oView.byId("inpu8lbl").setRequired(false);
					oView.byId("inpu19lbl").setRequired(false);

					oView.byId("TeshirSecBut").setVisible(false);
					oView.byId("MerkeziKontrolBut").setVisible(false);
					break;
				case 'ZA15':
					oVisible.setProperty("/iskonto1", true);
					oVisible.setProperty("/iskonto2", false);

					oView.byId("inpu1").setEditable(false);
					oView.byId("inpu2").setEditable(true);
					oView.byId("inpu201").setVisible(true);
					oView.byId("inpu16").setEditable(false);
					oView.byId("inpu19").setEditable(false);
					oView.byId("inpu20").setEditable(false);
					oView.byId("inpu26").setEditable(false);
					oView.byId("inpu27").setEditable(false);
					oView.byId("inpu28").setEditable(false);
					oView.byId("inpu34").setEditable(false);
					oView.byId("inpu35").setEditable(false);
					oView.byId("inpu36").setEditable(false);
					oView.byId("inpu37").setEditable(false);
					oView.byId("inpu38").setEditable(false);
					oView.byId("inpu39").setEditable(false);
					oView.byId("inpu40").setEditable(false);
					oView.byId("inpu42").setEditable(false);
					oView.byId("inpu43").setEditable(false);

					oView.byId("inpu25").setVisible(true);
					oView.byId("inpu25lbl").setVisible(true);
					oView.byId("inpu251").setVisible(true);

					oView.byId("inpu8lbl").setRequired(true);
					oView.byId("inpu19lbl").setRequired(false);

					oView.byId("TeshirSecBut").setVisible(true);
					oView.byId("MerkeziKontrolBut").setVisible(false);
					break;
			}

			// if (oGuid !== "00000000-0000-0000-0000-000000000000") {
			// 	oView.byId("inpu1").setEditable(false);
			// 	oView.byId("inpu2").setEditable(false);
			// 	oView.byId("inpu16").setEditable(false);
			// 	oView.byId("inpu19").setEditable(false);
			// 	oView.byId("inpu20").setEditable(false);

			// 	oView.byId("inpu26").setEditable(false);
			// 	oView.byId("inpu27").setEditable(false);
			// 	oView.byId("inpu28").setEditable(false);
			// 	oView.byId("inpu21").setEditable(false);
			// 	oView.byId("inpu29").setEditable(false);
			// 	oView.byId("inpu30").setEditable(false);
			// 	oView.byId("inpu34").setEditable(false);
			// 	oView.byId("inpu35").setEditable(false);
			// 	oView.byId("inpu36").setEditable(false);
			// 	oView.byId("inpu37").setEditable(false);
			// 	oView.byId("inpu38").setEditable(false);
			// 	oView.byId("inpu39").setEditable(false);
			// 	oView.byId("inpu40").setEditable(false);

			// 	if (oStat !== "E0002") {
			// 		oView.byId("inpu3").setEditable(false);
			// 		oView.byId("inpu4").setEditable(false);
			// 		oView.byId("inpu5").setEditable(false);
			// 		oView.byId("inpu6").setEditable(false);
			// 		oView.byId("inpu7").setEditable(false);
			// 		oView.byId("inpu8").setEditable(false);
			// 		oView.byId("inpu11").setEditable(false);
			// 		oView.byId("inpu14").setEditable(false);
			// 		oView.byId("inpu15").setEditable(false);
			// 		oView.byId("inpu17").setEditable(false);
			// 		oView.byId("inpu18").setEditable(false);
			// 		oView.byId("inpu22").setEditable(false);
			// 		oView.byId("inpu23").setEditable(false);
			// 		oView.byId("inpu24").setEditable(false);
			// 		oView.byId("inpu31").setEditable(false);
			// 		oView.byId("inpu32").setEditable(false);
			// 		oView.byId("inpu33").setEditable(false);
			// 		oView.byId("inpu41").setEditable(false);
			// 		oView.byId("inpu42").setEditable(false);
			// 		oView.byId("inpu43").setEditable(false);
			// 		oView.byId("inpu44").setEditable(false);
			// 		oView.byId("inpu25").setEditable(false);
			// 		oView.byId("inpu251").setEditable(false);
			// 	}
			// } else {
			// 	if (oOrderType === "ZS20") {
			// 		oView.byId("inpu19").setVisible(false);
			// 		oView.byId("inpu20").setVisible(false);
			// 	} else {
			// 		oView.byId("inpu19").setVisible(true);
			// 		oView.byId("inpu20").setVisible(true);
			// 	}

			// }
		},
		enableElements: function() {
			var oView = this.getView();

			oView.byId("inpu2").setVisible(true);
			oView.byId("inpu201").setVisible(true);
			oView.byId("inpu3").setVisible(true);
			oView.byId("inpu4").setVisible(true);
			oView.byId("inpu5").setVisible(true);
			oView.byId("inpu6").setVisible(true);
			oView.byId("inpu7").setVisible(true);
			oView.byId("inpu8").setVisible(true);
			// oView.byId("inpu11").setVisible(true);
			oView.byId("inpu14").setVisible(true);
			oView.byId("inpu15").setVisible(true);
			oView.byId("inpu16").setVisible(true);
			oView.byId("inpu17").setVisible(true);
			oView.byId("inpu18").setVisible(true);
			oView.byId("inpu19").setVisible(true);
			oView.byId("inpu20").setVisible(true);
			oView.byId("inpu21").setVisible(true);
			oView.byId("inpu22").setVisible(true);
			oView.byId("inpu23").setVisible(true);
			oView.byId("inpu24").setVisible(true);
			oView.byId("inpu25").setVisible(true);
			oView.byId("inpu251").setVisible(true);
			oView.byId("inpu26").setVisible(true);
			oView.byId("inpu27").setVisible(true);
			oView.byId("inpu28").setVisible(true);
			oView.byId("inpu29").setVisible(true);
			oView.byId("inpu30").setVisible(true);
			oView.byId("inpu31").setVisible(true);
			oView.byId("inpu32").setVisible(true);
			oView.byId("inpu33").setVisible(true);
			oView.byId("inpu34").setVisible(true);
			oView.byId("inpu35").setVisible(true);
			oView.byId("inpu36").setVisible(true);
			oView.byId("inpu37").setVisible(true);
			oView.byId("inpu38").setVisible(true);
			oView.byId("inpu39").setVisible(true);
			oView.byId("inpu40").setVisible(true);
			oView.byId("inpu41").setVisible(true);
			oView.byId("inpu42").setVisible(true);
			oView.byId("inpu43").setVisible(true);
			oView.byId("inpu44").setVisible(true);

			oView.byId("IconDeger").setVisible(true);
			oView.byId("IconKamp").setVisible(true);
			oView.byId("IconTahsilat").setVisible(true);
			oView.byId("IconDegisim").setVisible(true);
			oView.byId("idTahsScroll").setVisible(true);
			oView.byId("IconNotlar").setVisible(true);

			oView.byId("inpu2").setEditable(true);
			oView.byId("inpu3").setEditable(true);
			oView.byId("inpu4").setEditable(true);
			oView.byId("inpu5").setEditable(true);
			oView.byId("inpu6").setEditable(true);
			oView.byId("inpu7").setEditable(true);
			oView.byId("inpu8").setEditable(true);
			// oView.byId("inpu11").setEditable(true);
			oView.byId("inpu14").setEditable(true);
			oView.byId("inpu15").setEditable(true);
			oView.byId("inpu16").setEditable(true);
			oView.byId("inpu17").setEditable(true);
			oView.byId("inpu18").setEditable(true);
			oView.byId("inpu19").setEditable(true);
			oView.byId("inpu20").setEditable(true);
			oView.byId("inpu21").setEditable(true);
			oView.byId("inpu22").setEditable(true);
			oView.byId("inpu23").setEditable(true);
			oView.byId("inpu24").setEditable(true);
			oView.byId("inpu25").setEditable(true);
			oView.byId("inpu251").setEditable(true);
			oView.byId("inpu26").setEditable(true);
			oView.byId("inpu27").setEditable(true);
			oView.byId("inpu28").setEditable(true);
			//oView.byId("inpu29").setEditable(true);
			oView.byId("inpu30").setEditable(true);
			oView.byId("inpu31").setEditable(true);
			oView.byId("inpu32").setEditable(true);
			oView.byId("inpu33").setEditable(true);
			oView.byId("inpu34").setEditable(true);
			oView.byId("inpu35").setEditable(true);
			oView.byId("inpu36").setEditable(true);
			oView.byId("inpu37").setEditable(true);
			oView.byId("inpu38").setEditable(true);
			oView.byId("inpu39").setEditable(true);
			oView.byId("inpu40").setEditable(true);
			oView.byId("inpu41").setEditable(true);
			oView.byId("inpu42").setEditable(true);
			oView.byId("inpu43").setEditable(true);
			oView.byId("inpu44").setEditable(true);

		},

		bindElements: function(oData) {
			var oView = this.getView();

			var oPartners = oData.ToPartners.results;
			var oTexts = oData.ToTexts.results;

			if (this.oCustomer) {
				oView.byId("inpu2").setValue(this.oCustomer);
			}

			for (var i = 0; i < oPartners.length; i++) {
				if (oPartners[i].PartnerFct === "00000001") {
					oView.byId("inpu2").setValue(oPartners[i].PartnerNo);
					oView.byId("inpu3").setSelectedKey(oPartners[i].AddressShort);
					oView.byId("inpu201").setValue(oPartners[i].DescriptionName);
				}
				// if (oPartners[i].PartnerFct === "00000015") {
				// 	oView.byId("inpu11").setValue(oPartners[i].PartnerNo);
				// }
				if (oPartners[i].PartnerFct === "00000002") {
					oView.byId("inpu61").setValue(oPartners[i].AddressShort);
				}
			}

			for (i = 0; i < oTexts.length; i++) {
				if (oTexts[i].Tdid === "0001") {
					oView.byId("inpu44").setValue(oTexts[i].ConcLines);
				}
			}

			if (oData.Guid === "00000000-0000-0000-0000-000000000000") {
				oView.byId("idTitle").setText("Kayıt Yarat");
				// Yeni kayıtlarda bazı alanlar default gelecek.
				oView.byId("inpu21").setDateValue(new Date());
				oView.byId("inpu22").setDateValue(new Date());
				oView.byId("inpu23").setDateValue(new Date());
			} else {
				oView.byId("idTitle").setText("Kayıt Güncelle");
			}

			if (oData.ToPricing.Currency === "") oData.ToPricing.Currency = "TRY";

		},

		toBack: function(oEvent) {

			// ekrandan çıkarken yapılacak işlemler
			// sap.ui.core.UIComponent.getRouterFor(this).navTo("Targetmain", true);
			var oView = this.getView();
			oView.getModel("oModel").setData(null);
			oView.getModel("oModel").refresh();
			oView.getModel("KampanyaBelirle").setData(null);
			oView.getModel("KampanyaBelirle").refresh();
			oView.getModel("MerkeziKontrol").setData(null);
			oView.getModel("MerkeziKontrol").refresh();
			oView.getModel("oRefId").setData(null);
			oView.getModel("oRefId").refresh();

			if (sap.ui.getCore().getModel("oModelGlobal")) {
				sap.ui.getCore().setModel(null, "oModelGlobal");
			}

			oView.byId("inpu2").setValueState(sap.ui.core.ValueState.None);
			oView.byId("inpu4").setValueState(sap.ui.core.ValueState.None);
			oView.byId("inpu8").setValueState(sap.ui.core.ValueState.None);
			oView.byId("inpu14").setValueState(sap.ui.core.ValueState.None);
			oView.byId("inpu24").setValueState(sap.ui.core.ValueState.None);
			oView.byId("inpu25").setValueState(sap.ui.core.ValueState.None);
			oView.byId("inpu251").setValueState(sap.ui.core.ValueState.None);

			oView.byId("inpu2").setValue("");
			oView.byId("inpu201").setValue("");
			oView.byId("inpu3").setSelectedKey("");
			oView.byId("inpu5").setValue("");
			// oView.byId("inpu11").setValue("");
			oView.byId("inpu44").setValue("");
			oView.byId("inpu61").setValue("");

			history.back();
		},
		handleItemEnter: function(oEvent) {
			// sepette bir veri girip entera basınca o satırdaki ürünle ilgili veriler beckendden dönecek. 
			// bunun için yine onSave metodunu çağırıyoruz. ancak oradaki save alanı boş olacak şekilde gönderiyoruz
			var sPath = oEvent.getSource().getParent().getBindingContextPath();
			var oInd = parseInt(sPath.substring(sPath.lastIndexOf('/') + 1)); //gets the index of the listitem
			// var oInd = oEvent.getSource().getParent().getBindingContextPath().slice(-1);
			var oTab = this.getView().byId("SepetTabId").getModel("oModel").getProperty("/ToItems/results");
			if (oTab[oInd].OrderedProd !== "" && oTab[oInd].Quantity > "0.000") {
				var selectMusteri = this.getView().byId("inpu201").getValue();
				var musterivisible = this.getView().byId("inpu201").getVisible();
				var selectFiyatListesi = this.getView().byId("inpu25").getSelectedKey();
				var selectFiyatListesivisible = this.getView().byId("inpu25").getVisible();
				if (musterivisible) {
					if (!selectMusteri) {
						this.getView().getController().showMessageBox("Müşteri seçiniz!", "error");
						return;
					}
				}
				if (selectFiyatListesivisible) {
					if (!selectFiyatListesi) {
						this.getView().getController().showMessageBox("Fiyat listesi seçiniz!", "error");
						return;
					}
				}
				this.onSave(false, function() {
					this.recalculateAllFields();
					var tahsindoncesi = this.getView().getModel("oModel").getProperty("/Zzfld000057");
					// this.getView().getModel("oModel").setProperty("/ToTahsilat/results/0/Zzfld00007y", tahsindoncesi);
					// this.getView().getModel("oModel").setProperty("/ToTahsilat/results/0/Zzfld0000bp", tahsindoncesi);
					this.recalculateAllFields();
				}.bind(this));

				// 	if (this.oOrderType === "ZA01") {
				// 	var oModel = this.getView().getModel(oModel);
				// 	var oCode = "0005"; //ödeme tipi
				//     var	oExtension = this.oOrderType;
				// 	this.BaseValueHelp("/valueHelpSet", "/OdemeTipi", {
				// 		Code: oCode,
				// 		Extension: oExtension
				// 	});
				// }
			}
		},
		onPressSave: function() {
			this.onSave(true);
		},

		onSave: function(oSave, onRequestResult) {

			var oView = this.getView();

			var oModel = this.getView().getModel("oModel");
			var oKapampanyaBelirle = this.getView().getModel("KampanyaBelirle").getProperty("/");
			var oMerkeziKontrol = this.getView().getModel("MerkeziKontrol").getProperty("/");
			var oMerkeziKontrolButton = oModel.oData.MerkeziKontrol;
			var oStokSorguButton = oModel.oData.StokSorgu;
			var oEnYuksekTarihButton = oModel.oData.EnYuksekTarih;

			if (this.alanKontrol(oSave) === "true" || !oSave) { // zorunlu alan kontrolü

				if (oKapampanyaBelirle === 'X' || oModel.oData.KampanyaBelirle === 'X') {

					oModel.setProperty("/KampanyaBelirle", "X");
				}

				if (oMerkeziKontrol === 'X' || oMerkeziKontrolButton === 'X') {

					oModel.setProperty("/MerkeziKontrol", "X");
				}

				var oHeaderSet = this.clone(oModel.oData);

				// deep entity i oluşturmak için doğrudan modeli kullanıyorum. ancak bazı alanlar sıkıntı yaratıyor. bunları siliyorum.
				delete oHeaderSet.__metadata;

				delete oHeaderSet.ToStatus.__metadata;
				delete oHeaderSet.ToCustomerExt.__metadata;
				delete oHeaderSet.ToPricing.__metadata;
				delete oHeaderSet.ToCumulate.__metadata;
				delete oHeaderSet.ToCondEasyEntries.__metadata;
				delete oHeaderSet.ToCondEasyEntries.__deferred;
				delete oHeaderSet.ToSales.__metadata;
				delete oHeaderSet.ToSales.__deferred;

				delete oHeaderSet.ToOrgman;
				delete oHeaderSet.ToDates;

				oHeaderSet.ToCondEasyEntries.Header = oHeaderSet.Guid;
				oHeaderSet.ToCustomerExt.Guid = oHeaderSet.Guid;
				oHeaderSet.ToCumulate.Guid = oHeaderSet.Guid;
				oHeaderSet.ToPricing.Guid = oHeaderSet.Guid;
				oHeaderSet.ToSales.Guid = oHeaderSet.Guid;
				oHeaderSet.ToCumulate.Guid = oHeaderSet.Guid;

				if (oSave === true) oHeaderSet.Save = "X";

				if (oHeaderSet.ToPartners.results) {
					oHeaderSet.ToPartners = oHeaderSet.ToPartners.results;
					oHeaderSet.ToTexts = oHeaderSet.ToTexts.results;
				}

				this.getData(oHeaderSet);

				// checkboxlar işaretli ise X göndereceğiz.
				if (oView.byId("inpu15").getSelected()) {
					oHeaderSet.Zzfld00008e = "X";
				} else {
					oHeaderSet.Zzfld00008e = "";
				}
				if (oView.byId("inpu34").getSelected()) {
					oHeaderSet.DistributedI1006 = "X";
				} else {
					oHeaderSet.DistributedI1006 = "";
				}
				if (oView.byId("inpu41").getSelected()) {
					oHeaderSet.ToCustomerExt.Zzfld0000ag = "X";
				} else {
					oHeaderSet.ToCustomerExt.Zzfld0000ag = "";
				}

				//*********************************************
				//skarakas 
				//sözleşmedeki müşteri adı satınalma siparişindeki müşteri 
				//alanına yazılacak.
				if (oHeaderSet.ToCustomerExt.Zzfld00009o === "") {
					for (var i = 0; i < oHeaderSet.ToPartners.length; i++) {
						if (oHeaderSet.ToPartners[i].PartnerFct === "00000001") {
							oHeaderSet.ToCustomerExt.Zzfld00009o = oHeaderSet.ToPartners[i].DescriptionName;
						}
					}
				}
				//*********************************************

				oHeaderSet.ToItems = oHeaderSet.ToItems.results; //Sepet;
				oHeaderSet.ToTahsilat = oHeaderSet.ToTahsilat.results; //Tahsilat;
				oHeaderSet.ToMessages = oHeaderSet.ToMessages.results;
				if (oHeaderSet.ToDocflow) {

					oHeaderSet.ToDocflow = oHeaderSet.ToDocflow.results;
				} else {
					// oHeaderSet.ToDocflow = [];
					delete oHeaderSet.ToDocflow;
				}

				if (oHeaderSet.ToConfig && oHeaderSet.ToConfig.results) {
					oHeaderSet.ToConfig = oHeaderSet.ToConfig.results.map(function removeMetaData(line) {
						var oLine = Object.assign({}, line);
						delete oLine.__metadata;
						delete oLine.__oKarakteristik;
						return oLine;
					});
					// oHeaderSet.ToConfig.forEach(function (oLine) {
					// 	delete oLine.__metadata;
					// 	delete oLine.__oKarakteristik;
					// });
				} else {
					oHeaderSet.ToConfig = [];
				}
				// delete oHeaderSet.ToConfig;

				// tahsilat tablosundaki checkboxlar
				// for (var j = 0; j < oHeaderSet.ToTahsilat.length; j++) {
				// 	if (oHeaderSet.ToTahsilat[j].Zzfld00009h === true) {
				// 		oHeaderSet.ToTahsilat[j].Zzfld00009h = "X";
				// 	} else {
				// 		oHeaderSet.ToTahsilat[j].Zzfld00009h = "";
				// 	}
				// 	if (oHeaderSet.ToTahsilat[j].Zzfld0000bf === true) {
				// 		oHeaderSet.ToTahsilat[j].Zzfld0000bf = "X";
				// 	} else {
				// 		oHeaderSet.ToTahsilat[j].Zzfld0000bf = "";
				// 	}
				// }

				for (var j = 0; j < oHeaderSet.ToTahsilat.length; j++) {
					if (oHeaderSet.ToTahsilat[j].Zzfld00007x == "" || oHeaderSet.ToTahsilat[j].Zzfld00007x == null) {
						oHeaderSet.ToTahsilat[j].Zzfld00007x = "0.00";
					}
					if (oHeaderSet.ToTahsilat[j].Zzfld00007w == "" || oHeaderSet.ToTahsilat[j].Zzfld00007w == null) {
						oHeaderSet.ToTahsilat[j].Zzfld00007w = "0.00";
					}
				}

				if (oHeaderSet.ToCustomerExt.ZzSptiskonto == "" || oHeaderSet.ToCustomerExt.ZzSptiskonto == null) {
					oHeaderSet.ToCustomerExt.ZzSptiskonto = "0.00";
				}

				if (oHeaderSet.ToCustomerExt.Zzfld0000ay == "" || oHeaderSet.ToCustomerExt.Zzfld0000ay == null) {
					oHeaderSet.ToCustomerExt.Zzfld0000ay = "0.00";
				}

				this.oSave = oHeaderSet.Save;

				var oDataModel = this.getView().byId("SepetTabId").getModel();
				var that = this;
				var oBusyDialog = new sap.m.BusyDialog();
				oBusyDialog.open();
				oDataModel.create("/headerSet", oHeaderSet, {
					success: function(oData, oResponse) {

						var FollowupItems = this.getView().getModel("selected").getProperty("/FollowupItems", "X");
						var oModelJsonList = new sap.ui.model.json.JSONModel();
						if (oStokSorguButton || oMerkeziKontrolButton || oEnYuksekTarihButton) {
							that.showMessages(oData.ToMessages);
						}
						that.fixTabs(oData);
						// oData.ToConfig = oModel.getProperty("/ToConfig");
						oModelJsonList.setData(oData);
						oView.setModel(oModelJsonList, "oModel");
						if (oData.ToConfig && oData.ToConfig.results) {
							oData.ToConfig.results.forEach(function(oLine) {
								if (!oLine.__oKarakteristik) {
									this.loadConfigSearchHelp(oModelJsonList, oLine);
								}
							}.bind(this));
						}
						this.onOdemeKosulu();

						oBusyDialog.close();
						if (that.oSave === "X") {
							this.getView().getController().showMessageBox("Kayıt işlemi başarıyla tamamlanmıştır.", "success", function() {
								sap.ui.core.UIComponent.getRouterFor(this).navTo("detail", {
									guid: oData.Guid
								}, true);
							}.bind(this));
						} else {
							if (onRequestResult) {
								onRequestResult(oData);
							}

						}
					}.bind(this),
					error: function(oError) {
						oBusyDialog.close();
						this.getView().getController().showMessageBox(oError);
					}.bind(this)
				});
			} else {
				var oMessage = "Lütfen Tüm Zorunlu Alanları Doldurunuz";
				sap.m.MessageBox.show(oMessage, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "UYARI",
					actions: [sap.m.MessageBox.Action.OK]
				});
			}

		},
		clone: function(obj) {
			if (null === obj || "object" != typeof obj) return obj;
			var copy = obj.constructor();
			for (var attr in obj) {
				if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
			}
			return copy;
		},

		getData: function(oHeaderSet) {
			// doğrudan modele bağlı olmayan bazı alanlar var. onların değerlerini alıp modele atacağız
			var oView = this.getView();
			var oPartners1 = {};
			var oPartners2 = {};
			var oPartners3 = {};
			var oText = {};
			var oPartner1 = false;
			var oPartner2 = false;
			var oPartner7 = false;
			var oText1 = false;
			debugger;
			for (var i = 0; i < oHeaderSet.ToPartners.length; i++) {
				delete oHeaderSet.ToPartners[i].__metadata;
				if (oHeaderSet.ToPartners[i].PartnerFct === "00000001") {
					oHeaderSet.ToPartners[i].PartnerNo = oView.byId("inpu2").getValue();
					oHeaderSet.ToPartners[i].AddressShort = oView.byId("inpu3").getSelectedKey();
					oPartner1 = true;
				}
				if (oHeaderSet.ToPartners[i].PartnerFct === "00000002") {

					if (oHeaderSet.ProcessType === "ZS20") {

						oHeaderSet.ToPartners[i].PartnerNo = oView.byId("inpu2").getValue();
						oHeaderSet.ToPartners[i].AddressShort = oView.byId("inpu3").getSelectedKey();
						oPartner2 = true;

					} else {

						if (oView.byId("inpu5").getSelectedKey() !== "") {
							oHeaderSet.ToPartners[i].PartnerNo = oView.byId("inpu5").getSelectedKey();
						} else {
							oHeaderSet.ToPartners[i].PartnerNo = oView.byId("inpu2").getValue();
						}
						oHeaderSet.ToPartners[i].AddressShort = oView.byId("inpu61").getValue();
						oPartner2 = true;
					}
				}
				// if (oHeaderSet.ToPartners[i].PartnerFct === "00000015") {
				// 	oHeaderSet.ToPartners[i].PartnerNo = oView.byId("inpu11").getValue();
				// 	oPartner7 = true;
				// }
			}

			if (!oPartner1) {
				oPartners1.PartnerFct = "00000001";
				oPartners1.PartnerNo = oView.byId("inpu2").getValue();
				oPartners1.Guid = oHeaderSet.Guid;
				oPartners1.AddressShort = oView.byId("inpu3").getSelectedKey();
				oHeaderSet.ToPartners.push(oPartners1);
			}
			if (!oPartner2) {
				oPartners2.PartnerFct = "00000002";

				if (oHeaderSet.ProcessType === "ZS20") {

					oPartners2.PartnerNo = oView.byId("inpu2").getValue();
					oPartners2.Guid = oHeaderSet.Guid;
					oPartners2.AddressShort = oView.byId("inpu3").getSelectedKey();
					oHeaderSet.ToPartners.push(oPartners2);

				} else {
					if (oView.byId("inpu5").getSelectedKey() !== "") {
						oPartners2.PartnerNo = oView.byId("inpu5").getSelectedKey();
					} else {
						oPartners2.PartnerNo = oView.byId("inpu2").getValue();
					}

					oPartners2.Guid = oHeaderSet.Guid;
					oPartners2.AddressShort = oView.byId("inpu61").getValue();
					oHeaderSet.ToPartners.push(oPartners2);
				}

			}
			// if (!oPartner7 && oView.byId("inpu11") !== "") {
			// 	oPartners3.PartnerFct = "00000015";
			// 	oPartners3.PartnerNo = oView.byId("inpu11").getValue();
			// 	oPartners3.Guid = oHeaderSet.Guid;
			// 	oPartners3.DescriptionName = oView.byId("inpu11").getValue();
			// 	oHeaderSet.ToPartners.push(oPartners3);
			// }

			for (i = 0; i < oHeaderSet.ToTexts.length; i++) {
				delete oHeaderSet.ToTexts[i].__metadata;
				if (oHeaderSet.ToTexts[i].Tdid === "0001") {
					oHeaderSet.ToTexts[i].ConcLines = oView.byId("inpu44").getValue();
					oText1 = true;
				}
			}

			if (!oText1) {
				oText.Tdid = "0001";
				oText.ConcLines = oView.byId("inpu44").getValue();
				oHeaderSet.ToTexts.push(oText);
			}
		},

		toAddSepet: function() {
			// sepet tablosuna yeni satır ekleme
			if (this.getView().getModel("oModel").oData.ToStatus.Status === 'E0002' || this.oGuid ===
				"00000000-0000-0000-0000-000000000000") {
				var defaultDepo = "";
				var oModel = this.getView().getModel("oModel");
				var oModelDepo = this.getView().getModel("oDepo");
				var oDepos = oModelDepo.getProperty("/results");

				if (oDepos !== null) {
					for (var i = 0; i < oDepos.length; i++) {
						if (oDepos[i].Key === "0002") {
							defaultDepo = "0002";
						}
					}
				}
				var date = oModel.getProperty("/ToSales/ReqDlvDate");
				var oRow = {
					OrderedProd: "",
					Description: "",
					Quantity: "1.000",
					ProcessQtyUnit: "ADT",
					Conditionamount1: "0.000",
					Brmfytkdvli: "0.000",
					KampInd: "0.000",
					ThslatInd: "0.000",
					Zzfld00008x: "0.000",
					Currency: this.getView().byId("inpu251").getSelectedKey(),
					TeslimatTrh: date,
					// TeslimatTrh: this.getView().byId("inpu24").getDateValue(),
					Zzerpstatus: "",
					Zzspecad: "",
					Zzteshirdepo: defaultDepo,
					ShowConfigButton: "X"
				};
				var oTab = this.getView().byId("SepetTabId");
				var List = oTab.getModel("oModel").getProperty("/ToItems/results");
				List.push(oRow);
				oTab.getModel("oModel").setProperty("/ToItems/results", List);
				if (List.length === 1) {
					this.recalculateAllFields();
					this.toAddTahsilat();

					var tahsindoncesi = this.getView().getModel("oModel").getProperty("/Zzfld000057");
					this.getView().getModel("oModel").setProperty("/ToTahsilat/results/0/Zzfld00007y", tahsindoncesi);
					this.getView().getModel("oModel").setProperty("/ToTahsilat/results/0/Zzfld0000bp", tahsindoncesi);
				}
				oTab.getModel("oModel").refresh();

			} else {
				var oMessage = "Yalnızca E0002 statüsü için sepet verileri değiştirilebilir.";
				sap.m.MessageBox.show(oMessage, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "UYARI",
					actions: [sap.m.MessageBox.Action.OK]
				});
			}

		},
		deleteSepetRow: function(oEvent) {
			//sepet tablosundan satır silme
			if (this.getView().getModel("oModel").oData.ToStatus.Status === 'E0002' || this.oGuid ===
				"00000000-0000-0000-0000-000000000000") {

				var confSpliceFirst, confSpliceLast, configArrLength, configArr = [];

				if (this.getView().getModel("oModel").oData.ToConfig != null && this.getView().getModel("oModel").oData.ToConfig != undefined) {
					configArr = this.getView().getModel("oModel").oData.ToConfig.results;
					configArrLength = configArr.length;
				}

				var numIntDel = oEvent.getParameter("listItem").getBindingContext("oModel").getProperty().NumberInt;
				var itemsArr = this.getView().getModel("oModel").oData.ToItems.results;

				// for (var iItems = 0; iItems < itemsArr.length; iItems++) {
				// 	var sItemNo = itemsArr[iItems].NumberInt;
				// 	if (+sItemNo === +numIntDel) {
				// 		// oTab.getModel("oModel").oData.ToConfig.results.splice(iConf, 1);
				// 		// iConf--;
				// 	} else if (+sItemNo > +numIntDel) {
				// 		itemsArr[iItems].NumberInt = (+sItemNo - 10).toString().padStart(sItemNo.length, "0");
				// 	}
				// }

				var oTab = this.getView().byId("SepetTabId");
				var path = oEvent.getParameter("listItem").getBindingContext("oModel").getPath();
				var idx = parseInt(path.substring(path.lastIndexOf("/") + 1));
				oTab.getModel("oModel").oData.ToItems.results.splice(idx, 1);
				oTab.getModel("oModel").refresh();

				for (var iConf = 0; iConf < configArr.length; iConf++) {
					var sItemNoForConfig = configArr[iConf].ItemNoForConfig;
					if (+sItemNoForConfig === +numIntDel) {
						oTab.getModel("oModel").oData.ToConfig.results.splice(iConf, 1);
						iConf--;
						// } else if (+sItemNoForConfig > +numIntDel) {
						// 	configArr[iConf].ItemNoForConfig = (+sItemNoForConfig - 10).toString().padStart(sItemNoForConfig.length, "0");
					}
				}

				this.onSave(false, function() {
					this.recalculateAllFields();
					var tahsindoncesi = this.getView().getModel("oModel").getProperty("/Zzfld000057");
					this.recalculateAllFields();
				}.bind(this));

				//this.onSave(false);
				oTab.getModel("oModel").refresh();

			} else {
				var oMessage = "Yalnızca E0002 statüsü için sepet verileri değiştirilebilir.";
				sap.m.MessageBox.show(oMessage, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "UYARI",
					actions: [sap.m.MessageBox.Action.OK]
				});
			}

		},
		alanKontrol: function(oSave) {
			// zorunlu alan kontrolü
			var oView = this.getView();
			var oVal = "true";
			if (oSave) {
				if (!oView.byId("inpu201").getValue() && oView.byId("inpu201").getEditable() && oView.byId("inpu201").getVisible()) {
					oView.byId("inpu2").setValueState(sap.ui.core.ValueState.Error);
					oVal = "false";
				} else {
					oView.byId("inpu2").setValueState(sap.ui.core.ValueState.None);
				}
				// if (!oView.byId("inpu2").getValue() && oView.byId("inpu2").getEditable() && oView.byId("inpu2").getVisible()) {
				// 	oView.byId("inpu2").setValueState(sap.ui.core.ValueState.Error);
				// 	oVal = "false";
				// } else {
				// 	oView.byId("inpu2").setValueState(sap.ui.core.ValueState.None);
				// }
				if (!oView.byId("inpu4").getSelectedKey() && oView.byId("inpu4").getEditable() && oView.byId("inpu4").getVisible()) {
					oView.byId("inpu4").setValueState(sap.ui.core.ValueState.Error);
					oVal = "false";
				} else {
					oView.byId("inpu4").setValueState(sap.ui.core.ValueState.None);
				}
				if (this.oOrderType !== "ZS20") {
					if (!oView.byId("inpu8").getValue() && oView.byId("inpu8").getEditable() && oView.byId("inpu8").getVisible()) {
						oView.byId("inpu8").setValueState(sap.ui.core.ValueState.Error);
						oVal = "false";
					} else {
						oView.byId("inpu8").setValueState(sap.ui.core.ValueState.None);
					}
				}
				if (this.oOrderType === "ZA01") {
					if (!oView.byId("inpu19").getValue() && oView.byId("inpu19").getEditable() && oView.byId("inpu19").getVisible()) {
						oView.byId("inpu19").setValueState(sap.ui.core.ValueState.Error);
						oVal = "false";
					} else {
						oView.byId("inpu19").setValueState(sap.ui.core.ValueState.None);
					}
				}
				if (!oView.byId("inpu14").getValue() && oView.byId("inpu14").getEditable() && oView.byId("inpu14").getVisible()) {
					oView.byId("inpu14").setValueState(sap.ui.core.ValueState.Error);
					oVal = "false";
				} else {
					oView.byId("inpu14").setValueState(sap.ui.core.ValueState.None);
				}
				if (!oView.byId("inpu24").getDateValue() && oView.byId("inpu24").getEditable() && oView.byId("inpu24").getVisible()) {
					oView.byId("inpu24").setValueState(sap.ui.core.ValueState.Error);
					oVal = "false";
				} else {
					oView.byId("inpu24").setValueState(sap.ui.core.ValueState.None);
				}
				if (!oView.byId("inpu25").getValue() && oView.byId("inpu25").getEditable() && oView.byId("inpu25").getVisible()) {
					oView.byId("inpu25").setValueState(sap.ui.core.ValueState.Error);
					oVal = "false";
				} else {
					oView.byId("inpu25").setValueState(sap.ui.core.ValueState.None);
				}
				if (!oView.byId("inpu251").getSelectedKey() && oView.byId("inpu251").getEditable() && oView.byId("inpu251").getVisible()) {
					oView.byId("inpu251").setValueState(sap.ui.core.ValueState.Error);
					oVal = "false";
				} else {
					oView.byId("inpu251").setValueState(sap.ui.core.ValueState.None);
				}
			}

			return oVal;
		},
		editableFormatter: function(oValue) {
			if (this.oGuid === "00000000-0000-0000-0000-000000000000") {
				return true;
			} else {
				if (!oValue) {
					return true;
				} else {
					return false;
				}
			}
		},

		editableFormatter2: function(oValue) {
			if (this.getView().getModel("oModel").oData.Guid === "00000000-0000-0000-0000-000000000000") {
				return true;
			} else {
				if (this.getView().getModel("oModel").oData.ToStatus.Status === 'E0002' || !oValue) {
					return true;
				} else {
					return false;
				}
			}
		},
		editableFormatterT: function(oOdemeId, oKasa) {
			if (this.oGuid === "00000000-0000-0000-0000-000000000000") {
				return true;
			} else {
				if (!oOdemeId) {
					return true;
				} else {
					return false;
				}
			}
		},
		editableFormatterT2: function(oOdemeId, oKasa, oOdemeTip) {
			if (this.oGuid === "00000000-0000-0000-0000-000000000000") {
				return true;
			} else {
				if (oOdemeTip === "13") {
					return false;
				} else {
					if (!oOdemeId) {
						return true;
					} else if (oKasa !== true) {
						return true;
					} else {
						return false;
					}
				}

			}
		},

		editableFormatterKasa: function(oKasa) {

			if (this.oGuid === "00000000-0000-0000-0000-000000000000") {
				return true;
			} else {
				if (oKasa === true) {
					return false;
				} else {
					return true;
				}
			}
		},

		editableFormatterTip1: function(oOdemeTip) {
			if (oOdemeTip === "04" || oOdemeTip === "05" || oOdemeTip === "") {
				return false;
			} else {
				return true;

			}
		},
		editableFormatterTip2: function(oOdemeTip) {
			if (oOdemeTip === "06" || oOdemeTip === "08" || oOdemeTip === "") {
				return false;
			} else {
				return true;
			}
		},
		formatterToplam: function(o1, o2) {
			var newNum = Number(o1) + Number(o2);

			return newNum.toFixed(2);

			// return Number(o1) + Number(o2);
		},

		formatterBirlestir: function(o1, o2, o3) {
			var oSonuc = "";

			if (o1) oSonuc = o1;
			if (o2) oSonuc += o2;
			if (o3) oSonuc += o3;

			return oSonuc;
		},

		toAddTahsilat: function() {
			// tahsilat tablosuna satır ekler

			var oOdenecekTutar = 0;
			var oToplamTutar = 0;
			var oOdenenTutar = 0;

			var oTip = "OT-10";

			var oTab = this.getView().byId("TahsilatTabId");
			var oTahsilatList = oTab.getModel("oModel").getProperty("/ToTahsilat");
			var oSepetList = this.getView().byId("SepetTabId").getModel("oModel").getProperty("/ToItems/results");

			if (oSepetList) var oLength = oSepetList.length;
			else oLength = 0;

			// Sepet tablosundaki toplam tutar tahsilat tablosundaki ödenecek tutar alanına yazılacak.
			// for (var i = 0; i < oLength; i++) {
			// 	// oToplamTutar = parseFloat(oToplamTutar) + parseFloat(oSepetList[i].Brmfytkdvli);
			// 	oToplamTutar = parseFloat(oToplamTutar) + parseFloat(oSepetList[i].Zzfld00008x);

			// }
			oToplamTutar = this.getView().getModel("oModel").getProperty("/Zzfld000057");

			if (oTahsilatList) oLength = oTahsilatList.results.length;
			else {
				oLength = 0;
				oTahsilatList = {
					results: []
				};
			}
			if (oLength === 0) {
				oOdenecekTutar = oToplamTutar;
			} else {
				oTip = "OT-" + ((oLength + 1) * 10);

				for (var i = 0; i < oLength; i++) {
					// if (oTahsilatList.results[i].Zzfld000044 !== "01" && oTahsilatList.results[i].Zzfld000044 !== "02" &&
					// 	oTahsilatList.results[i].Zzfld000044 !== "03" && oTahsilatList.results[i].Zzfld000044 !== "10" &&
					// 	oTahsilatList.results[i].Zzfld000044 !== "11" && oTahsilatList.results[i].Zzfld000044 !== "") {

					if (!oTahsilatList.results[i].Zzfld00005w) {
						oOdenenTutar = parseFloat(oOdenenTutar) + parseFloat(oTahsilatList.results[i].Zzfld00007y);
					}
					// }

				}
				oOdenecekTutar = parseFloat(oToplamTutar) - parseFloat(oOdenenTutar);
			}
			var parseoOdenecekTutar = oOdenecekTutar.toString();
			// if (oOdenecekTutar === 0) {
			// 	oOdenecekTutar = "0.000";
			// }

			if (typeof(oOdenecekTutar) == 'string') {
				oOdenecekTutar = parseFloat(oOdenecekTutar).toFixed(2);
			} else {
				oOdenecekTutar = oOdenecekTutar.toFixed(2);
			}

			oToplamTutar = parseFloat(oToplamTutar).toFixed(2);

			var oRow = {
				Zzfld00005v: oTip,
				Zzfld00005w: "",
				Zzfld000085: "",
				Zzfld000044: "", ////
				Zzfld00007u: "",
				Zzfld000081: "",
				Zzfld0000an: "",
				Zzfld0000at: "",
				Zzfld0000c2: "",
				//Zzfld000047: new Date( ),
				Zzfld000045: "",
				Zzfld00007y: oOdenecekTutar + "",
				Zzfld0000bp: oOdenecekTutar + "",
				// Zzfld00007y: parseoOdenecekTutar,
				// Zzfld0000bp: parseoOdenecekTutar,
				Zzfld00007w: "0.000",
				Zzfld00007x: "0.000",
				Zzfld00009h: false,
				Zzfld0000bf: false
			};

			oTahsilatList.results.push(oRow);
			oTab.getModel("oModel").setProperty("/ToTahsilat", oTahsilatList);
			// this.onTahsilatBilgisiHesapla();
			oTab.getModel("oModel").refresh();
			this.recalculateAllFields();
		},

		deleteTahsilatRow: function(oEvent) {
			// tahsilat tablosundan satır sil
			var oTab = this.getView().byId("TahsilatTabId");
			var path = oEvent.getParameter("listItem").getBindingContext("oModel").getPath();
			var idx = parseInt(path.substring(path.lastIndexOf("/") + 1));

			if (oTab.getModel("oModel").oData.ToTahsilat.results[idx].Zzfld00009h === "" ||
				oTab.getModel("oModel").oData.ToTahsilat.results[idx].Zzfld00009h === false) {
				oTab.getModel("oModel").oData.ToTahsilat.results.splice(idx, 1);
				// this.onTahsilatBilgisiHesapla();
				oTab.getModel("oModel").refresh();
			} else {
				var oMessage = "Kasa kontrol işaretli olan kayıtlar silinemez.";
				sap.m.MessageBox.show(oMessage, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "UYARI",
					actions: [sap.m.MessageBox.Action.OK]
				});
			}
			this.recalculateAllFields();
		},

		handleTahsilatEnter: function(oEvent) {

			var sPath = oEvent.getSource().getParent().getBindingContextPath();
			var oInd = parseInt(sPath.substring(sPath.lastIndexOf('/') + 1)); //gets the index of the listitem
			//	var oInd = oEvent.getSource().getParent().getBindingContextPath().slice(-1);
			var oModel = this.getView().byId("TahsilatTabId").getModel("oModel");
			var oTab = oModel.getProperty("/ToTahsilat/results");
			oTab[oInd].Zzfld0000bp = ((+oTab[oInd].Zzfld00007y || 0) + (+oTab[oInd].Zzfld00007x || 0)) + "";
			oModel.setProperty("/ToTahsilat/results", oTab);
		},
		handleItemEnterCombo: function(oEvent) {
			// this.onSave(false);
			this.getView().getModel("oModel").setProperty("/FiyatListeChange", "X");
			this.onSave(false, function() {
				this.recalculateAllFields();
			}.bind(this));
		},

		onTahsilatDate: function(oEvent) {
			var path = oEvent.getSource().getParent().oBindingContexts.oModel.sPath;
			var p = oEvent.getSource().getParent().oBindingContexts.oModel.getProperty(path);
			var arr = oEvent.getSource().getValue().split(".");
			arr[1]--;
			var d = new Date(arr[2], arr[1], arr[0]);
			d.setDate(d.getDate() + 1);
			p.Zzfld000047 = d;
		},

		onOdemeTurChange: function(oEvent) {
			var selectedItemNull = "";
			var source = oEvent.getSource();
			var selectedItem = oEvent.getSource().getSelectedItem().getKey();
			if (selectedItem === "13") {
				var oIndArr = oEvent.getSource().getParent().getBindingContextPath().split('/');
				var oInd = oIndArr[3];
				var oModel = this.getView().byId("TahsilatTabId").getModel("oModel");
				var oTab = oModel.getProperty("/ToTahsilat/results");
				MessageBox.show("Sanal Pos Ödeme Tipini Seçemezsiniz!", {
					icon: MessageBox.Icon.ERROR,
					title: "Error",
					actions: [MessageBox.Action.CLOSE],
					defaultAction: MessageBox.Action.CLOSE,
					onClose: function(sButton) {
						if (sButton === MessageBox.Action.CLOSE) {
							//execute my logic in here => that works
							oTab[oInd].Zzfld000044 = "";
							oModel.setProperty("/ToTahsilat/results", oTab);
							// source.setSelectedItem(selectedItemNull, true, true);
						}
					}
				});
			} else {

				// tahsilat tablosunda seçilen ödeme türüne göre tahsilat durumu değişecek
				//var oInd = oEvent.getSource().getParent().getBindingContextPath().slice(-2);
				var oIndArr = oEvent.getSource().getParent().getBindingContextPath().split('/');
				var oInd = oIndArr[3];
				var oModel = this.getView().byId("TahsilatTabId").getModel("oModel");
				var oTab = oModel.getProperty("/ToTahsilat/results");

				if (oTab[oInd].Zzfld000044 !== "11" && oTab[oInd].Zzfld000044 !== " ") {
					oTab[oInd].Zzfld000047 = new Date();
				}
				if (oTab[oInd].Zzfld000044 === "01" ||
					oTab[oInd].Zzfld000044 === "02" ||
					oTab[oInd].Zzfld000044 === "03" ||
					oTab[oInd].Zzfld000044 === "10" ||
					oTab[oInd].Zzfld000044 === "12" ||
					oTab[oInd].Zzfld000044 === "14" ||
					oTab[oInd].Zzfld000044 === "11") {
					oTab[oInd].Zzfld000045 = "01";
				} else if (oTab[oInd].Zzfld000044 === "04" ||
					oTab[oInd].Zzfld000044 === "05" ||
					oTab[oInd].Zzfld000044 === "06" ||
					oTab[oInd].Zzfld000044 === "07" ||
					oTab[oInd].Zzfld000044 === "08") {
					oTab[oInd].Zzfld000045 = "02";
				}

				if (oTab[oInd].Zzfld000044 === "04" && (oTab[oInd].RecordId === "" || oTab[oInd].RecordId === null || oTab[oInd].RecordId ===
						undefined)) {
					oTab[oInd].Zzfld0000at = "";
				}

				oModel.setProperty("/ToTahsilat/results", oTab);

				if (oTab[oInd].Zzfld000044 === "01" ||
					oTab[oInd].Zzfld000044 === "02" ||
					oTab[oInd].Zzfld000044 === "03" ||
					oTab[oInd].Zzfld000044 === "10" ||
					oTab[oInd].Zzfld000044 === "12" ||
					oTab[oInd].Zzfld000044 === "14" ||
					oTab[oInd].Zzfld000044 === "11") {

					var oModelRef = this.getView().getModel("oRefId");

					var oId = {
						Key: oTab[oInd].Zzfld00005v
					};

					if (oModelRef) {

						var oAddNewId = true;
						var oDat = this.getView().getModel("oRefId").getProperty("/results");

						if (oDat !== null) {
							for (var i = 0; i < oDat.length; i++) {
								if (oDat[i].Key === oId.Key) {
									oAddNewId = false;
								}
							}
						} else {
							oDat = {
								results: []
							};
							oDat.results.push(oId);
							var oModelJsonList = new sap.ui.model.json.JSONModel(oDat);
							this.getView().setModel(oModelJsonList, "oRefId");
							oAddNewId = false;
						}

						if (oAddNewId) {
							oDat.push(oId);
							oModelRef.setProperty("/results", oDat);
						}

					} else {
						oDat = {
							results: []
						};
						oDat.results.push(oId);
						var oModelJsonList = new sap.ui.model.json.JSONModel(oDat);
						this.getView().setModel(oModelJsonList, "oRefId");
					}

				}

				this.recalculateAllFields();
			}
		},

		fillRefIdOnEdit: function() {

			var oModel = this.getView().byId("TahsilatTabId").getModel("oModel");
			var oTab = oModel.getProperty("/ToTahsilat/results");
			var oModelRef = this.getView().getModel("oRefId");
			if (oModelRef) {
				this.getView().getModel("oRefId").setData(null);
				this.getView().getModel("oRefId").refresh();
			}
			for (var i = 0; i < oTab.length; i++) {
				if (oTab[i].Zzfld000044 === "01" ||
					oTab[i].Zzfld000044 === "02" ||
					oTab[i].Zzfld000044 === "03" ||
					oTab[i].Zzfld000044 === "10" ||
					oTab[i].Zzfld000044 === "12" ||
					oTab[i].Zzfld000044 === "14" ||
					oTab[i].Zzfld000044 === "11") {

					var oModelRef = this.getView().getModel("oRefId");

					var oId = {
						Key: oTab[i].Zzfld00005v
					};

					if (oModelRef) {
						var oDat = this.getView().getModel("oRefId").getProperty("/results");
						if (oDat !== null) {
							oDat.push(oId);
							oModelRef.setProperty("/results", oDat);
						} else {
							oDat = {
								results: []
							};
							oDat.results.push(oId);
							var oModelJsonList = new sap.ui.model.json.JSONModel(oDat);
							this.getView().setModel(oModelJsonList, "oRefId");
						}

					} else {
						oDat = {
							results: []
						};
						oDat.results.push(oId);
						var oModelJsonList = new sap.ui.model.json.JSONModel(oDat);
						this.getView().setModel(oModelJsonList, "oRefId");
					}

				}
			}

			oModel.setProperty("/ToTahsilat/results", oTab);

			//	this.recalculateAllFields();
		},

		editFalseTahsilat: function() {

			var oModel = this.getView().byId("TahsilatTabId").getModel("oModel");
			var oTab = oModel.getProperty("/ToTahsilat/results");

			for (var i = 0; i < oTab.length; i++) {
				if (oTab[i].Zzfld000044 === "13") {

				}
			}

			oModel.setProperty("/ToTahsilat/results", oTab);

			//	this.recalculateAllFields();

		},

		onTahsDurumChange: function(oEvent) {

			var sPath = oEvent.getSource().getParent().getBindingContextPath();
			var oInd = parseInt(sPath.substring(sPath.lastIndexOf('/') + 1)); //gets the index of the listitem
			// var oInd = oEvent.getSource().getParent().getBindingContextPath().slice(-1);
			var oModel = this.getView().byId("TahsilatTabId").getModel("oModel");
			var oTab = oModel.getProperty("/ToTahsilat/results");

			if (oTab[oInd].Zzfld000045 === "02" &&
				(oTab[oInd].Zzfld000044 === "01" ||
					oTab[oInd].Zzfld000044 === "02" ||
					oTab[oInd].Zzfld000044 === "03" ||
					oTab[oInd].Zzfld000044 === "10" ||
					oTab[oInd].Zzfld000044 === "11")) {

				for (var i = 0; i < oTab.length; i++) {
					if (oTab[i].Zzfld00005w === oTab[oInd].Zzfld00005v) {
						oTab[i].Zzfld000045 = "02";
					}
				}

			}

			oModel.setProperty("/ToTahsilat/results", oTab);

		},

		onBayiChange: function(oEvent) {
			// bayi seçimine göre adres arama yardımı değişecek. 

			var val = this.getView().byId("inpu5").getSelectedKey();
			var oAdres = this.getView().byId("inpu6");
			var oModelJsonList = new sap.ui.model.json.JSONModel();
			var oFilters = [];
			var oFilter = new Filter("Code", FilterOperator.EQ, "0003");
			oFilters.push(oFilter);
			oFilter = new Filter("Extension", FilterOperator.EQ, val);
			oFilters.push(oFilter);
			this.getView().getModel().read("/valueHelpSet", {
				filters: oFilters,
				success: function(oData, response) {
					oModelJsonList.setData(oData);
					oAdres.setModel(oModelJsonList, "oAdres");
				},
				error: function(oError) {}
			});
		},
		onSelectFromProd: function(oEvent) {

			// Teşhirden Satış Mağaza ZA15
			if (this.oOrderType === "ZA15") {
				if (!this._oSelProdHelpDialog) {
					this._oSelProdHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_sales_order_operation.fragments.selectFromProd", this);
					this.getView().addDependent(this._oSelProdHelpDialog);
					this._oSelProdHelpDialog.open();
				} else {
					this._oSelProdHelpDialog.open();
				}
			} else {
				sap.m.MessageBox.show("Teşhirden seçme yalnızca Teşhirden Satış Mağaza - ZA15 türündeki siparişlerde geçerlidir", {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "HATA",
					actions: [sap.m.MessageBox.Action.OK]
				});
			}

		},

		handleSelectProdCancel: function(oEvent) {
			this._oSelProdHelpDialog.close();
			sap.ui.getCore().byId("idSipNo").setValue("");
			sap.ui.getCore().byId("idKalemNo").setValue("");
			sap.ui.getCore().byId("idUrunKod").setValue("");
			sap.ui.getCore().byId("idUrunTanim").setValue("");
			this._oSelProdHelpDialog.getModel("oTeshir").setProperty("/", {});
			this.onSave(false);
		},
		handleSelectProdSearch: function(oEvent) {
			// teşhirden seç ekranı girilen verilere göre tablo çekiliyor
			var oModelProd = this.getView().getModel();
			var oSipNo = sap.ui.getCore().byId("idSipNo").getValue();
			var oKalemNo = sap.ui.getCore().byId("idKalemNo").getValue();
			var oUrunKod = sap.ui.getCore().byId("idUrunKod").getValue();
			var oUrunTanim = sap.ui.getCore().byId("idUrunTanim").getValue();

			var oBusyDialog = new sap.m.BusyDialog();

			var oFilters = [];

			if (this.oGuid !== "00000000-0000-0000-0000-000000000000") {
				oFilters.push(new Filter({
					path: 'OrderGuid',
					value1: this.oGuid,
					operator: FilterOperator.EQ
				}));
			}

			if (oSipNo) {
				oFilters.push(new Filter({
					path: 'SiparisNo',
					value1: oSipNo,
					operator: FilterOperator.EQ
				}));
			}

			if (oKalemNo) {
				oFilters.push(new Filter({
					path: 'KalemNo',
					value1: oKalemNo,
					operator: FilterOperator.EQ
				}));
			}
			if (oUrunKod) {
				oFilters.push(new Filter({
					path: 'UrunKodu',
					value1: oUrunKod,
					operator: FilterOperator.EQ
				}));
			}
			if (oUrunTanim) {
				oFilters.push(new Filter({
					path: 'UrunTanimi',
					value1: oUrunTanim,
					operator: FilterOperator.EQ
				}));
			}

			var oModelJsonList = new sap.ui.model.json.JSONModel();
			var that = this;
			oBusyDialog.open();
			oModelProd.read("/teshirdenSecSet", {
				filters: oFilters,
				success: function(oData, response) {
					that.stockFilter(oData);
					oModelJsonList.setData(oData);
					that._oSelProdHelpDialog.setModel(oModelJsonList, "oTeshir");
					oBusyDialog.close();
				},
				error: function(oError) {
					oBusyDialog.close();
				}
			});

		},

		stockFilter: function(oData) {

			var oSepetList = this.getView().getModel("oModel").getProperty("/ToItems/results");

			for (var i = 0; i < oSepetList.length; i++) {
				if (oSepetList[i].ZzaTeshirblgno !== "" && oSepetList[i].Zzfld000083 !== "") {
					for (var j = 0; j < oData.results.length; j++) {
						if (oData.results[j].SiparisNo === oSepetList[i].ZzaTeshirblgno && oData.results[j].KalemNo === oSepetList[i].Zzfld000083) {
							oData.results[j].Stok = oData.results[j].Stok - oSepetList[i].Quantity;
							if (oData.results[j].Stok <= 0) {
								oData.results.splice(j, 1);
							}
						}
					}
				}
			}

			return oData;
		},

		handleSelectProdAdd: function(oEvent) {
			// teşhirden seçilen ürünler sepet tablosuna yansıtılır
			var oTab = this.getView().byId("SepetTabId");
			var List = oTab.getModel("oModel").getProperty("/ToItems/results");
			var oSelProdTab = sap.ui.getCore().byId("SelProdTabId");
			var oSelected = oSelProdTab.getSelectedItems();

			for (var i = 0; i < oSelected.length; i++) {

				var oInd = oSelected[i].getBindingContextPath().split('/')[2];
				var oSipNo = oSelProdTab.getItems()[oInd].getCells()[0].getText();
				var oKalemNo = oSelProdTab.getItems()[oInd].getCells()[1].getText();
				var oUrunKod = oSelProdTab.getItems()[oInd].getCells()[2].getText();
				var oIndirim = oSelProdTab.getItems()[oInd].getCells()[5].getText();

				var defaultDepo = "";

				var oModelDepo = this.getView().getModel("oDepo");
				var oDepos = oModelDepo.getProperty("/results");

				if (oDepos !== null) {
					for (var depoCount = 0; depoCount < oDepos.length; depoCount++) {
						if (oDepos[depoCount].Key === "0002") {
							defaultDepo = "0002";
						}
					}
				}
				var oRow = {
					OrderedProd: oUrunKod,
					Description: "",
					Quantity: "1.000",
					ProcessQtyUnit: "ADT",
					Conditionamount1: "0.000",
					Brmfytkdvli: "0.000",
					KampInd: "0.000",
					ThslatInd: "0.000",
					Zzfld00008x: "0.000",
					Currency: "",
					Zzerpstatus: "",
					Zzspecad: "",
					Zzteshirdepo: defaultDepo,
					ShowConfigButton: "X",
					Zzfld000083: oKalemNo,
					ZzaTeshirblgno: oSipNo,
					ZzTeshirind: oIndirim
				};

				List.push(oRow);

			}

			oTab.getModel("oModel").setProperty("/ToItems/results", List);
			oTab.getModel("oModel").refresh();
			this._oProdHelpDialog.close();
		},

		// onProdValueHelp: function (oEvent) {
		// 	// ürün arama yardımı
		// 	if (!this._oProdHelpDialog) {
		// 		this._oProdHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_sales_order_operation.fragments.ProdSearch", this);
		// 		this.getView().addDependent(this._oProdHelpDialog);
		// 		this._oProdHelpDialog.open();
		// 	} else {
		// 		this._oProdHelpDialog.open();
		// 	}

		// 	this.Source = oEvent.getSource();

		// },

		handleProdCancel: function(oEvent) {
			this._oProdHelpDialog.close();
		},
		handleProdSearch: function(oEvent) {

			var oModelProd = this.getView().getModel("PROD");

			var oProdId = sap.ui.getCore().byId("idProdId").getValue();
			var oProdDes = sap.ui.getCore().byId("idProdDesc").getValue();
			var oCatId = sap.ui.getCore().byId("idCatId").getValue();
			var oCatDes = sap.ui.getCore().byId("idCatDesc").getValue();
			var oRow = sap.ui.getCore().byId("idProdRows").getValue();

			var oBusyDialog = new sap.m.BusyDialog();

			var oFilters = [];
			if (oProdId !== "") {
				var oFilter = new Filter("ProductId", FilterOperator.EQ, oProdId);
				oFilters.push(oFilter);
			}
			if (oProdDes !== "") {
				oFilter = new Filter("ShortText", FilterOperator.EQ, oProdDes);
				oFilters.push(oFilter);
			}
			if (oCatId !== "") {
				oFilter = new Filter("CategoryId", FilterOperator.EQ, oCatId);
				oFilters.push(oFilter);
			}
			if (oCatDes !== "") {
				oFilter = new Filter("CategoryDesc", FilterOperator.EQ, oCatDes);
				oFilters.push(oFilter);
			}
			if (oRow !== "") {
				oFilter = new Filter("MaxRows", FilterOperator.EQ, oRow);
				oFilters.push(oFilter);
			}

			var oModelJsonList = new sap.ui.model.json.JSONModel();
			var that = this;
			oBusyDialog.open();
			oModelProd.read("/productSearchSet", {
				filters: oFilters,
				success: function(oData, response) {
					oModelJsonList.setData(oData);
					that._oProdHelpDialog.setModel(oModelJsonList, "oProd");
					oBusyDialog.close();
				},
				error: function(oError) {
					oBusyDialog.close();
				}
			});

		},

		handleProdListItemPress: function(oEvent) {
			// ürün arama yardımından bir satır seçilince ürün kodu ve metni yazılır. 
			var oInd = oEvent.getParameter("listItem").getBindingContextPath().split('/')[2];
			var oProd = sap.ui.getCore().byId("ProdTabId").getItems()[oInd].getCells()[0].getText();
			this.Source.setValue(oProd);
			// Ürünün metnini de tanım alanına basmamız gerekiyor. 
			// var oProdText = sap.ui.getCore().byId("ProdTabId").getItems()[oInd].getCells()[1].getText();
			// var oTextId = this.Source.getId();
			// oTextId = oTextId.substr(0, 7) + "1" + oTextId.substr(8);
			// sap.ui.getCore().byId(oTextId).setValue(oProdText);
			this._oProdHelpDialog.close();
		},
		onCatValueHelp: function(oEvent) {
			// katalog arama yardımı
			if (!this._oCatHelpDialog) {
				this._oCatHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_sales_order_operation.fragments.CatSearch", this);
				this.getView().addDependent(this._oCatHelpDialog);
				this._oCatHelpDialog.open();
			} else {
				this._oCatHelpDialog.open();
			}

		},

		handleCatCancel: function() {
			this._oCatHelpDialog.close();
		},

		handleCatSearch: function() {

			var oModelProd = this.getView().getModel("PROD");

			var oCatId = sap.ui.getCore().byId("idCategId").getValue();
			var oCatDes = sap.ui.getCore().byId("idCategDesc").getValue();
			var oHyrId = sap.ui.getCore().byId("idHyrType").getSelectedKey();
			var oHyrDes = sap.ui.getCore().byId("idHyrDes").getValue();
			var oRow = sap.ui.getCore().byId("idCatRows").getValue();

			var oBusyDialog = new sap.m.BusyDialog();

			var oFilters = [];
			if (oCatId !== "") {
				var oFilter = new Filter("CategoryId", FilterOperator.EQ, oCatId);
				oFilters.push(oFilter);
			}
			if (oCatDes !== "") {
				oFilter = new Filter("CategoryText", FilterOperator.EQ, oCatDes);
				oFilters.push(oFilter);
			}
			if (oHyrId !== "") {
				oFilter = new Filter("HierarchyId", FilterOperator.EQ, oHyrId);
				oFilters.push(oFilter);
			}
			if (oHyrDes !== "") {
				oFilter = new Filter("HierarchyText", FilterOperator.EQ, oHyrDes);
				oFilters.push(oFilter);
			}
			if (oRow !== "") {
				oFilter = new Filter("MaxRows", FilterOperator.EQ, oRow);
				oFilters.push(oFilter);
			}

			var oModelJsonList = new sap.ui.model.json.JSONModel();
			var that = this;
			oBusyDialog.open();
			oModelProd.read("/categorySearchSet", {
				filters: oFilters,
				success: function(oData, response) {
					oModelJsonList.setData(oData);
					that._oCatHelpDialog.setModel(oModelJsonList, "oCat");
					oBusyDialog.close();
				},
				error: function(oError) {
					oBusyDialog.close();
				}
			});

		},

		handleCatListItemPress: function(oEvent) {
			// katalog arama yardımından seçilen katalog ürün arama yardımındaki katalog alanına yazılır
			var oInd = oEvent.getParameter("listItem").getBindingContextPath().split('/')[2];
			var oCatid = sap.ui.getCore().byId("CatTabId").getItems()[oInd].getCells()[0].getText();
			sap.ui.getCore().byId("idCatId").setValue(oCatid);
			this._oCatHelpDialog.close();
		},
		// onCustomerValueHelp: function () {
		// 	// müşteri arama yardımı
		// 	if (!this._oValueHelpDialog) {
		// 		this._oValueHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_sales_order_operation.fragments.CustomerSearch", this);
		// 		this.getView().addDependent(this._oValueHelpDialog);
		// 		this._oValueHelpDialog.open();
		// 	} else {

		// 		this._oValueHelpDialog.open();
		// 	}

		// },

		handleCustCancel: function() {
			this._oValueHelpDialog.close();
		},
		handleCustomerSearch: function() {

			var oModelCust = this.getView().getModel("CUST");

			var oPartner = sap.ui.getCore().byId("idCustIdent").getValue();
			var oAd = sap.ui.getCore().byId("idCustName").getValue();
			var oSoyad = sap.ui.getCore().byId("idCustLastName").getValue();
			var oTel = sap.ui.getCore().byId("idCustTel").getValue();
			var oTur = sap.ui.getCore().byId("idCustType").getSelectedKey();
			var oBusyDialog = new sap.m.BusyDialog();

			var oFilters = [];
			if (oPartner !== "") {
				var oFilter = new Filter("Partner", FilterOperator.EQ, oPartner);
				oFilters.push(oFilter);
			}
			if (oAd !== "") {
				oFilter = new Filter("Firstname", FilterOperator.EQ, oAd);
				oFilters.push(oFilter);
			}
			if (oSoyad !== "") {
				oFilter = new Filter("Lastname", FilterOperator.EQ, oSoyad);
				oFilters.push(oFilter);
			}
			if (oTel !== "") {
				oFilter = new Filter("Telephonemob", FilterOperator.EQ, oTel);
				oFilters.push(oFilter);
			}
			if (oTur !== "") {
				oFilter = new Filter("Partnertype", FilterOperator.EQ, oTur);
				oFilters.push(oFilter);
			}

			var oModelJsonList = new sap.ui.model.json.JSONModel();
			var that = this;
			oBusyDialog.open();
			oModelCust.read("/searchCustomerSet", {
				filters: oFilters,
				success: function(oData, response) {
					oModelJsonList.setData(oData);
					that._oValueHelpDialog.setModel(oModelJsonList, "oCustomer");
					oBusyDialog.close();
				},
				error: function(oError) {
					oBusyDialog.close();
				}
			});

		},

		handleCustomerListItemPress: function(oEvent) {
			// müşteri arama yardımından dönen id ve ad soyad alanları alınır
			var oInd = oEvent.getParameter("listItem").getBindingContextPath().split('/')[2];
			var oPartner = sap.ui.getCore().byId("CustTabId").getItems()[oInd].getCells()[0].getText();
			var oPartnerName = sap.ui.getCore().byId("CustTabId").getItems()[oInd].getCells()[1].getText();
			this.getView().byId("inpu2").setValue(oPartner);
			this.getView().byId("inpu201").setValue(oPartnerName);

			var oFilters = [];

			// seçilen müşsteriye göre müşteri adresi arama yardımı dolar
			var oFilter = new Filter("Code", FilterOperator.EQ, "0014");
			oFilters.push(oFilter);
			oFilter = new Filter("Extension", FilterOperator.EQ, this.oOrderType);
			oFilters.push(oFilter);
			oFilter = new Filter("Extension2", FilterOperator.EQ, oPartner);
			oFilters.push(oFilter);

			var oMAdres = this.getView().byId("inpu3");
			var oModelJsonList = new sap.ui.model.json.JSONModel();
			this.getView().getModel().read("/valueHelpSet", {
				filters: oFilters,
				success: function(oData, response) {
					oModelJsonList.setData(oData);
					oMAdres.setModel(oModelJsonList, "oMusteriAdres");
				},
				error: function(oError) {

				}
			});

			this._oValueHelpDialog.close();
		},

		handleCustEnter: function() {
			// müşteri elle girilirse adını soyadını bulmak için aşağıdaki entity çağırılır
			var oPartner = this.getView().byId("inpu2").getValue();
			var oModelCust = this.getView().getModel("CUST");
			var oFilters = [];

			if (oPartner !== "") {
				var oFilter = new Filter("Partner", FilterOperator.EQ, oPartner);
				oFilters.push(oFilter);
			}

			var that = this;

			oModelCust.read("/searchCustomerSet", {
				filters: oFilters,
				success: function(oData, response) {
					if (oData.results.length > 0) {
						var oPartnerName = oData.results[0].FullName;
						// var oPartnerName = oData.results[0].Firstname + " " + oData.results[0].Lastname;
						that.getView().byId("inpu201").setValue(oPartnerName);
					} else {
						that.getView().byId("inpu201").setValue("");
					}
				}

			});
			// müşteri adresi için
			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0014");
			oFilters.push(oFilter);
			oFilter = new Filter("Extension", FilterOperator.EQ, this.oOrderType);
			oFilters.push(oFilter);
			oFilter = new Filter("Extension2", FilterOperator.EQ, oPartner);
			oFilters.push(oFilter);

			var oMAdres = this.getView().byId("inpu3");
			var oModelJsonList = new sap.ui.model.json.JSONModel();
			this.getView().getModel().read("/valueHelpSet", {
				filters: oFilters,
				success: function(oData, response) {
					oModelJsonList.setData(oData);
					this.getView().setModel(oModelJsonList, "oMusteriAdres");
					this.setOwnerModelProperty("selected", "/MusteriAdres", oData.results[0].Key);
					// oMAdres.setModel(oModelJsonList, "oMusteriAdres");
				}.bind(this),
				error: function(oError) {

				}
			});

			this.onSave(false);
			this.getView().byId("inpu2").setEnabled(false);
		},

		onItemConfig: function(oEvent) {
			// sepet tablosunda konfigürasyon butonuna basılınca konfigürasyon ekranı açılır.
			var sPath = oEvent.getSource().getBindingContext("oModel").getPath();
			var oInd = parseInt(sPath.substring(sPath.lastIndexOf('/') + 1)); //gets the index of the listitem
			var oItems = this.getView().getModel("oModel").getProperty("/ToItems/results");
			var oItem = oItems[oInd];
			var oFilters = [];
			var currentItemNumber;
			var oModel = this.getView().getModel("oModel");
			var aConfig = oModel.getProperty("/ToConfig/results") || [];
			var oModelJsonList = new sap.ui.model.json.JSONModel();
			oModelJsonList.setSizeLimit(99999);

			if (oItem.Parent && oItem.Parent !== "00000000-0000-0000-0000-000000000000") {
				var oFilter = new Filter("ParentId", FilterOperator.EQ, oItem.Parent);
				oFilters.push(oFilter);
			} else {
				currentItemNumber = String((parseInt(oInd, 10) + 1) * 10);
				currentItemNumber = oItem.NumberInt;
				oFilter = new Filter("ZzorderedProd", FilterOperator.EQ, oItem.OrderedProd);
				oFilters.push(oFilter);
				oFilter = new Filter("ItemNoForConfig", FilterOperator.EQ, currentItemNumber);

				oFilters.push(oFilter);
			}

			aConfig = aConfig.filter(function removeOldConfig(item) {
				return +item.ItemNoForConfig === +currentItemNumber;
			});

			if (!this._oConfigScreen) {
				this._oConfigScreen = sap.ui.xmlfragment("zcrm.zcrm_sales_order_operation.fragments.ItemConfig", this);
				this.getView().addDependent(this._oConfigScreen);
				this._oConfigScreen.open();
			} else {

				this._oConfigScreen.open();
			}
			var that = this;
			// item config bilgileri

			if (aConfig.length) {
				oModelJsonList.setData({
					results: aConfig
				});
				this.getOwnerComponent().setModel(oModelJsonList, "oConfig");
			} else {
				this.getView().getModel().read("/itemConfigSet", {
					filters: oFilters,
					success: function(oData, response) {
						oModelJsonList.setData(oData);

						this.getOwnerComponent().setModel(oModelJsonList, "oConfig");
						oData.results.forEach(function(oLine) {
							this.loadConfigSearchHelp(oModelJsonList, oLine);
						}.bind(this));
						this.bindCharacteristics(oData);
					}.bind(this),
					error: function(oError) {
						var message = oError.responseText;
						sap.m.MessageBox.show(message, {
							icon: sap.m.MessageBox.Icon.ERROR,
							title: "HATA",
							actions: [sap.m.MessageBox.Action.OK]
						});
					}
				});
			}

		},
		bindCharacteristics: function(oData) {

		},
		handleConfigSave: function(oEvent) {
			var oModel = this.getView().getModel("oModel");
			var aConfig = oModel.getProperty("/ToConfig/results") || [];
			var oConfigModel = this.getView().getModel("oConfig").getData().results;
			if (aConfig && oConfigModel) {
				var currentItemNumber = oConfigModel[0].ItemNoForConfig;

				aConfig = aConfig.filter(function removeOldConfig(item) {
					return item.ItemNoForConfig !== currentItemNumber;
				}).concat(oConfigModel);
			}
			this.getView().getModel("oModel").setProperty("/ToConfig", {
				results: aConfig
			});
			// this.onSave(false);
			this.onSave(false, function() {
				this.recalculateAllFields();

			}.bind(this));
			this._oConfigScreen.close();
		},
		handleConfigCancel: function() {
			this._oConfigScreen.close();
		},

		toSenetOlustur: function() {

			// tahsilat tablosunda senet oluştur butonuna basarsak girilen senet sayısı kadar tahsilat satırı oluşturur.
			// satırların tarihlerini birer ay artırır
			var message;
			var oCurDate = new Date();
			var oTab = this.getView().byId("TahsilatTabId");
			var oTahsilatList = oTab.getModel("oModel").getProperty("/ToTahsilat/results");

			var oRow = oTahsilatList[oTahsilatList.length - 1];

			if (this.getView().getModel("oModel").getProperty("/ToPricing").PriceList === "02") {
				message = "Kampanyalı fiyat üzerinden senetli satış yapılamaz!";
				sap.m.MessageBox.show(message, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "HATA",
					actions: [sap.m.MessageBox.Action.OK]
				});
			} else if (oRow.Zzfld0000c2 === "") {
				message = "Lütfen senet adedi giriniz";
				sap.m.MessageBox.show(message, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "HATA",
					actions: [sap.m.MessageBox.Action.OK]
				});
			} else if (oRow.Zzfld0000c2 > 12) {
				message = "En fazla 12 adet senet oluşturabilirsiniz";
				sap.m.MessageBox.show(message, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "HATA",
					actions: [sap.m.MessageBox.Action.OK]
				});
			} else if (!oRow.Zzfld0000c3) {
				message = "Senet oluşturmadan önce ilk senet tarihini giriniz";
				sap.m.MessageBox.show(message, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "HATA",
					actions: [sap.m.MessageBox.Action.OK]
				});
			} else if (Math.ceil(Math.abs(oRow.Zzfld0000c3.getTime() - oCurDate.getTime()) / (1000 * 60 * 60 * 24)) > 45) {
				message = "İlk senet tarihi 45 günden ileri bir tarih seçilemez";
				sap.m.MessageBox.show(message, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "HATA",
					actions: [sap.m.MessageBox.Action.OK]
				});
			} else {

				var oCount = oRow.Zzfld0000c2;
				var oTip;
				// var oSenetMakbuzNo;
				var oSenetCount = 0;
				var oSenetPrefix = "0";
				var oSenetNo;
				var oBirimMiktar = oRow.Zzfld00007y / oCount;
				// var ParseoBirimMiktar = oBirimMiktar.toString();
				var oTarih = oRow.Zzfld0000c3;
				var oYeniTarih = new Date(oTarih.valueOf());

				// if (oRow.Zzfld000044 === "11") {
				var oTDUrum = "01";
				// } else {
				// 	oTDUrum = "";
				// }

				while (oCount > 0) {
					oCount--;
					oSenetCount++;
					oTip = "OT-" + ((oTahsilatList.length + 1) * 10);
					if (oSenetCount < 10) {
						oSenetNo = oSenetPrefix + oSenetCount;
					} else {
						oSenetNo = oSenetCount.toString();
					}
					var oRowNew = {
						Zzfld00005v: oTip,
						Zzfld00005w: oRow.Zzfld00005v,
						Zzfld000085: "",
						Zzfld000044: "11", ////
						Zzfld00007u: "",
						Zzfld000081: "",
						Zzfld0000an: "",
						// Zzfld0000at: "",
						Zzfld0000at: oSenetNo,
						Zzfld0000c2: "",
						Zzfld0000c4: new Date(oYeniTarih),
						Zzfld000045: oTDUrum,
						Zzfld00007y: oBirimMiktar + "",
						Zzfld0000bp: oBirimMiktar + "",
						// Zzfld00007y: ParseoBirimMiktar,
						Zzfld00007w: "0.000",
						Zzfld00007x: "0.000",
						Zzfld00009h: false,
						Zzfld0000bf: false
					};
					oTahsilatList.push(oRowNew);
					oYeniTarih.setMonth(oYeniTarih.getMonth() + 1);

				}
				oTab.getModel("oModel").setProperty("/ToTahsilat/results", oTahsilatList);
				oTab.getModel("oModel").refresh();

			}

		},
		oPerSozlSelect: function() {
			// personel sözelşmesi tiki atılırsa bazı alanların bekendden default gelmesi gerekiyor.bunun için aşağıdaki seti kullanıyoruz.
			var oView = this.getView();

			var oHead = oView.getModel("oModel").getProperty("/");

			if (oView.byId("inpu15").getSelected() && oHead.ProcessType === "ZA04") {

				var oMusteri = this.getView().byId("inpu2").getValue();
				var oFilters = [];
				var oFilter = new Filter("Code", FilterOperator.EQ, "0002");
				oFilters.push(oFilter);
				oFilter = new Filter("Extension", FilterOperator.EQ, oMusteri);
				oFilters.push(oFilter);

				oView.getModel().read("/defaultValuesSet", {
					filters: oFilters,
					success: function(oData, response) {
						oHead.ZzPersiptop = oData.ZzPersiptop;
						oView.getModel("oModel").setProperty("/ZzPersiptop", oData.results[0].ZzPersiptop);
						oView.getModel("oModel").setProperty("/", oHead);
					},
					error: function(oError) {}
				});

			}

		},
		showMessages: function(oMessages) {
			// stok kontrole basıldığı zaman bekende gidiyoruz ve stok bilgileri toMessages içinde geliyor.
			// eğer toMessages doluysa popupla bu bilgilier gösterilir.
			if (oMessages) {
				if (oMessages.results.length > 0) {
					if (!this._oStockDialog) {
						this._oStockDialog = sap.ui.xmlfragment("zcrm.zcrm_sales_order_operation.fragments.StockControl", this);
						this.getView().addDependent(this._oStockDialog);
						this._oStockDialog.open();
					} else {

						this._oStockDialog.open();
					}
				}
			}
		},
		onTahsInd: function() {
			// özel iskonto veya bekleyen iskonto girilirse tahsilat indirimi öncesi ve tahsilat indirimi alanları aşağıdaki koşullara göre doldurulur. 
			// tahsilat tablosundaki indirim tutarı alanı da eklenir
			var oTahsInd;
			var oTahsIndSonra;

			var oView = this.getView();
			var oTahsIndOnce = oView.byId("inpu35").getValue();
			var oIsk = oView.byId("inpu32").getValue();
			var bIsk = oView.byId("inpu33").getValue();
			var oTahsilatList = oView.getModel("oModel").getProperty("/ToTahsilat/results");

			if (oIsk) {
				oTahsInd = oIsk;
			}

			if (bIsk) {
				oTahsInd = parseFloat(oTahsInd) + parseFloat(bIsk);
			}

			for (var i = 0; i < oTahsilatList.length; i++) {
				if (oTahsilatList[i].Zzfld00007x) oTahsInd = parseFloat(oTahsInd) + parseFloat(oTahsilatList[i].Zzfld00007x);
			}

			oView.byId("inpu37").setValue(oTahsInd);

			oTahsIndSonra = oTahsIndOnce - oTahsInd;
			oView.byId("inpu36").setValue(oTahsIndSonra);

		},

		handleStockCancel: function() {
			this._oStockDialog.close();
		},
		toKampanya: function() {
			// kampnaya belirle butonuna basılırsa modeldeki KampanyaBelirle alanı X yapılır ve bekende gönderilir.

			this.getView().getModel("KampanyaBelirle").setProperty("/", "X");
			this.onSave(false, function() {
				this.recalculateAllFields();
				var tahsindoncesi = this.getView().getModel("oModel").getProperty("/Zzfld000057");
				// this.getView().getModel("oModel").setProperty("/ToTahsilat/results/0/Zzfld00007y", tahsindoncesi);
				// this.getView().getModel("oModel").setProperty("/ToTahsilat/results/0/Zzfld0000bp", tahsindoncesi);
				this.recalculateAllFields();
			}.bind(this));
		},

		onMerkeziKontrol: function() {

			this.getView().getModel("MerkeziKontrol").setProperty("/", "X");
			this.getView().getModel("oModel").setProperty("/MerkeziKontrol", "X");
			// this.onSave(false);

			this.onSave(false, function() {
				this.recalculateAllFields();

			}.bind(this));
		},

		onPlanUygula: function() {
			this.getView().getModel("oModel").setProperty("/PlaniUygula", "X");
			// this.onSave(false);

			this.onSave(false, function() {
				this.recalculateAllFields();

			}.bind(this));
		},
		onStokKontrol: function() {
			this.getView().getModel("oModel").setProperty("/StokSorgu", "X");
			// this.onSave(false);

			this.onSave(false, function() {
				this.recalculateAllFields();

			}.bind(this));
		},

		onEnTarih: function() {
			this.getView().getModel("oModel").setProperty("/EnYuksekTarih", "X");
			// this.onSave(false);

			this.onSave(false, function() {
				this.recalculateAllFields();

			}.bind(this));
		},

		// onSelectFromCat: function () {

		// 	if (!this._oSelProdCatHelpDialog) {
		// 		this._oSelProdCatHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_sales_order_operation.fragments.selectFromCat", this);
		// 		this.getView().addDependent(this._oSelProdCatHelpDialog);
		// 		this._oSelProdCatHelpDialog.open();
		// 	} else {
		// 		this._oSelProdCatHelpDialog.open();
		// 	}
		// },

		handleSelectFromCatCancel: function(oEvent) {
			this._oSelProdCatHelpDialog.close();
		},
		handleSelectCatSearch: function(oEvent) {
			// ürün kataloğundan seç arama yardımı
			var oModelProd = this.getView().getModel("PROD");
			var oHead = this.getView().getModel("oModel").getProperty("/");

			var oTakim = sap.ui.getCore().byId("idTakim").getValue();
			var oModel = sap.ui.getCore().byId("idModel").getValue();
			var oModelTnm = sap.ui.getCore().byId("idTModelTnm").getValue();
			var oUniteUrunKod = sap.ui.getCore().byId("idUniteUrunKod").getValue();
			var oUrunId = sap.ui.getCore().byId("idUrunId").getValue();
			var oCategId = sap.ui.getCore().byId("idCategId").getValue();

			if (sap.ui.getCore().byId("idConfigEnable").getSelected()) var oConfigEnable = "X";

			var oBusyDialog = new sap.m.BusyDialog();

			var oFilters = [];

			if (oTakim) {
				oFilters.push(new Filter({
					path: 'Zztakim',
					value1: oTakim,
					operator: FilterOperator.EQ
				}));
			}

			if (oModelTnm) {
				oFilters.push(new Filter({
					path: 'CategoryDesc',
					value1: oModelTnm,
					operator: FilterOperator.EQ
				}));
			}
			if (oUniteUrunKod) {
				oFilters.push(new Filter({
					path: 'Description',
					value1: oUniteUrunKod,
					operator: FilterOperator.EQ
				}));
			}
			if (oModel) {
				oFilters.push(new Filter({
					path: 'Zzmodel',
					value1: oModel,
					operator: FilterOperator.EQ
				}));
			}
			if (oUrunId) {
				oFilters.push(new Filter({
					path: 'ProductId',
					value1: oUrunId,
					operator: FilterOperator.EQ
				}));
			}
			if (oCategId) {
				oFilters.push(new Filter({
					path: 'CategoryId',
					value1: oCategId,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ProcessType) {
				oFilters.push(new Filter({
					path: 'ProcessType',
					value1: oHead.ProcessType,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.PriceList) {
				oFilters.push(new Filter({
					path: 'PriceList',
					value1: oHead.ToPricing.PriceList,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.Currency) {
				oFilters.push(new Filter({
					path: 'Currency',
					value1: oHead.ToPricing.Currency,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.PriceDate) {
				oFilters.push(new Filter({
					path: 'PriceDate',
					value1: oHead.ToPricing.PriceDate,
					operator: FilterOperator.EQ
				}));
			}
			// if (oHead.ProcessType) {
			// 	oFilters.push(new Filter({
			// 		path: 'SoldToPartnerGuid',
			// 		value1: oConfigEnable,
			// 		operator: FilterOperator.EQ
			// 	}));
			// }

			var oModelJsonList = new sap.ui.model.json.JSONModel();
			var that = this;
			oBusyDialog.open();
			oModelProd.read("/productCatalogSearchSet", {
				filters: oFilters,
				success: function(oData, response) {
					oModelJsonList.setData(oData);
					that._oSelProdCatHelpDialog.setModel(oModelJsonList, "oCatalog");
					oBusyDialog.close();
				},
				error: function(oError) {
					oBusyDialog.close();
				}
			});

		},

		handleSelectCatAdd: function(oEvent) {
			// ürün kataloğundan birden fazla satır seçilebilir. seçtikten sonra popup kapatılmaz. seçilenler sepet tablosuna eklenir.
			var oTab = this.getView().byId("SepetTabId");
			var List = oTab.getModel("oModel").getProperty("/ToItems/results");
			var oSelProdTab = sap.ui.getCore().byId("SelProdCatTabId");
			var oSelected = oSelProdTab.getSelectedItems();
			var defaultDepo = "";

			var oModelDepo = this.getView().getModel("oDepo");
			var oDepos = oModelDepo.getProperty("/results");

			if (oDepos !== null) {
				for (var i = 0; i < oDepos.length; i++) {
					if (oDepos[i].Key === "0002") {
						defaultDepo = "0002";
					}
				}
			}
			for (var i = 0; i < oSelected.length; i++) {

				var oInd = oSelected[i].getBindingContextPath().split('/')[2];
				var oConfig = oSelProdTab.getItems()[oInd].getCells()[0].getText();
				var oUrunKod = oSelProdTab.getItems()[oInd].getCells()[4].getText();
				// var oIndirim = oSelProdTab.getItems()[oInd].getCells()[5].getText();

				var oRow = {
					OrderedProd: oUrunKod,
					Description: "",
					Quantity: "0.000",
					ProcessQtyUnit: "ADT",
					Conditionamount1: "0.000",
					Brmfytkdvli: "0.000",
					KampInd: "0.000",
					ThslatInd: "0.000",
					Zzfld00008x: "0.000",
					Currency: "",
					Zzerpstatus: "",
					Zzspecad: "",
					Zzteshirdepo: defaultDepo,
					ShowConfigButton: oConfig
						// Zzfld000083: oKalemNo,
						// ZzaTeshirblgno: oSipNo,
						// ZzTeshirind: oIndirim
				};

				List.push(oRow);

			}

			oTab.getModel("oModel").setProperty("/ToItems/results", List);
			oTab.getModel("oModel").refresh();
			//			this._oSelProdCatHelpDialog.close();
		},

		handleSelectProdTree: function() {
			// Ürün ağacından seç arama yardımı burada ürün katoloğundan seçilen satırlar kullanıarak ürün ağacı getiririlir.
			var oModelProd = this.getView().getModel("PROD");
			var oHead = this.getView().getModel("oModel").getProperty("/");
			var oSelProdTab = sap.ui.getCore().byId("SelProdCatTabId");
			var oSelected = oSelProdTab.getSelectedItems();
			var oFilterA = [];
			var oFilterProd = [];

			for (var i = 0; i < oSelected.length; i++) {

				var oInd = oSelected[i].getBindingContextPath().split('/')[2];
				var oUrunKod = oSelProdTab.getItems()[oInd].getCells()[4].getText();
				if (oUrunKod) {
					oFilterProd.push(new Filter({
						path: 'ProductId',
						value1: oUrunKod,
						operator: FilterOperator.EQ,
						and: false
					}));
				}
			}

			var oFilterOR = new Filter({
				filters: oFilterProd,
				and: false,
			});

			if (oHead.ProcessType) {
				oFilterA.push(new Filter({
					path: 'ProcessType',
					value1: oHead.ProcessType,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.PriceList) {
				oFilterA.push(new Filter({
					path: 'PriceList',
					value1: oHead.ToPricing.PriceList,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.Currency) {
				oFilterA.push(new Filter({
					path: 'Currency',
					value1: oHead.ToPricing.Currency,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.PriceDate) {
				oFilterA.push(new Filter({
					path: 'PriceDate',
					value1: oHead.ToPricing.PriceDate,
					operator: FilterOperator.EQ
				}));
			}

			var oFilterAND = new Filter({
				filters: oFilterA,
				and: true
			});
			var oFilters = [new Filter({
				and: true,
				filters: [oFilterOR, oFilterAND]

			})];

			var oBusyDialog = new sap.m.BusyDialog();
			var oModelJsonList = new sap.ui.model.json.JSONModel();
			var that = this;

			oBusyDialog.open();
			oModelProd.read("/productCatalogTreeSearchSet", {
				filters: oFilters,
				success: function(oData, response) {
					oModelJsonList.setData(oData);
					that._oSelProdTreeHelpDialog.setModel(oModelJsonList, "oProdTree");
					oBusyDialog.close();
				},
				error: function(oError) {
					oBusyDialog.close();
				}
			});

			if (!this._oSelProdTreeHelpDialog) {
				this._oSelProdTreeHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_sales_order_operation.fragments.SelectFromProdTree", this);
				this.getView().addDependent(this._oSelProdTreeHelpDialog);
				this._oSelProdTreeHelpDialog.open();
			} else {
				this._oSelProdTreeHelpDialog.open();
			}

		},
		handleSelectProdTreeCancel: function(oEvent) {
			this._oSelProdTreeHelpDialog.close();
		},
		handleSelectProdTreeAdd: function(oEvent) {
			// ürün ağacından seçilen satırlar sepet tablosuna eklenir.
			var oTab = this.getView().byId("SepetTabId");
			var List = oTab.getModel("oModel").getProperty("/ToItems/results");
			var oSelProdTab = sap.ui.getCore().byId("SelProdTreeTabId");
			var oSelected = oSelProdTab.getSelectedItems();
			var defaultDepo = "";
			var oModelDepo = this.getView().getModel("oDepo");
			var oDepos = oModelDepo.getProperty("/results");

			if (oDepos !== null) {
				for (var i = 0; i < oDepos.length; i++) {
					if (oDepos[i].Key === "0002") {
						defaultDepo = "0002";
					}
				}
			}
			for (var i = 0; i < oSelected.length; i++) {

				var oInd = oSelected[i].getBindingContextPath().split('/')[2];
				var oUrunKod = oSelProdTab.getItems()[oInd].getCells()[0].getText();
				var oText = oSelProdTab.getItems()[oInd].getCells()[1].getText();
				var oQuantity = oSelProdTab.getItems()[oInd].getCells()[5].getText();

				var oRow = {
					OrderedProd: oUrunKod,
					Description: oText,
					Quantity: oQuantity,
					ProcessQtyUnit: "",
					Conditionamount1: "0.000",
					Brmfytkdvli: "0.000",
					KampInd: "0.000",
					ThslatInd: "0.000",
					Zzfld00008x: "0.000",
					Currency: "",
					Zzerpstatus: "",
					Zzspecad: "",
					Zzteshirdepo: defaultDepo,
					ShowConfigButton: ""
				};
				List.push(oRow);
			}

			oTab.getModel("oModel").setProperty("/ToItems/results", List);
			oTab.getModel("oModel").refresh();
			this._oSelProdTreeHelpDialog.close();
		},

		fixTabs: function(data) {

			if (!data.ToTahsilat) {
				data.ToTahsilat = {
					results: []
				};
			}
			if (!data.ToPartners) {
				data.ToPartners = {
					results: []
				};
			}
			if (!data.ToMessages) {
				data.ToMessages = {
					results: []
				};
			}
			if (!data.ToItems) {
				data.ToItems = {
					results: []
				};
			}
			if (!data.ToTexts) {
				data.ToTexts = {
					results: []
				};
			}
			if (!data.ToDates) {
				data.ToDates = {
					results: []
				};
			}

		},
		// handleCreateCustomer: function () {

		// 	var oCore = sap.ui.getCore();

		// 	if (!this.CustomerCreate) {
		// 		this.CustomerCreate = sap.ui.xmlfragment("zcrm.zcrm_sales_order_operation.fragments.CreateCustomer", this);
		// 		this.getView().addDependent(this.CustomerCreate);
		// 		this.CustomerCreate.open();
		// 	} else {
		// 		this.CustomerCreate.open();
		// 	}
		// 	oCore.byId("idCustomerType").setSelectedKey("1");
		// 	oCore.byId("idCustomerTax").setVisible(false);
		// 	oCore.byId("idCustomerTaxNo").setVisible(false);
		// 	oCore.byId("idCustCompanyName").setVisible(false);

		// 	var oModel = this.getView().getModel();
		// 	var oView = this.getView();
		// 	var oFilters = [];
		// 	var oFilter = {};
		// 	oFilters = [];
		// 	oFilter = new Filter("Code", FilterOperator.EQ, "0012");
		// 	oFilters.push(oFilter);
		// 	oFilter = new Filter("Extension", FilterOperator.EQ, "1");
		// 	oFilters.push(oFilter);

		// 	oModel.read("/valueHelpSet", {
		// 		filters: oFilters,
		// 		success: function (oData, response) {
		// 			var oModelJsonList8 = new sap.ui.model.json.JSONModel(oData);
		// 			oView.setModel(oModelJsonList8, "oCustGrup");
		// 		},
		// 		error: function (oError) {}
		// 	});

		// 	var oModelCust = this.getView().getModel("CUST");

		// 	oModelCust.read("/searchHelpCountrySet", {
		// 		success: function (oData, response) {
		// 			var oModelJsonList = new sap.ui.model.json.JSONModel(oData);
		// 			oModelJsonList.setData(oData);
		// 			oModelJsonList.setSizeLimit(500);
		// 			oView.setModel(oModelJsonList, "oUlke");
		// 		}
		// 	});

		// 	oFilters = [];
		// 	oFilter = new Filter("Land1", FilterOperator.EQ, "TR");
		// 	oFilters.push(oFilter);
		// 	oModelCust.read("/searchHelpRegionSet", {
		// 		filters: oFilters,
		// 		success: function (oData, response) {
		// 			var oModelJsonList = new sap.ui.model.json.JSONModel(oData);
		// 			oModelJsonList.setData(oData);
		// 			oView.setModel(oModelJsonList, "oSehir");
		// 		}
		// 	});

		// },
		onCustomerCreateCancel: function() {
			this.CustomerCreate.close();
		},
		onCustomerCreateSave: function() {

			var oModelCust = this.getView().getModel("CUST");
			var oCore = sap.ui.getCore();

			if (this.custAlanKontrol()) {
				var oCustomer = {};

				oCustomer.Lifecyclestage = oCore.byId("idCustomerRole").getSelectedKey();
				oCustomer.BuGroup = oCore.byId("idGroup").getSelectedKey();
				oCustomer.Partnertype = oCore.byId("idCustomerType").getSelectedKey();

				if (oCustomer.Partnertype === "1") // Bireysel müşteri
				{
					oCustomer.Firstname = oCore.byId("idCustomerName").getValue();
					oCustomer.Lastname = oCore.byId("idCustomerSurName").getValue();
					oCustomer.TaxNum = oCore.byId("idCustomerID").getValue();
				} else {
					oCustomer.TaxNum = oCore.byId("idCustomerTaxNo").getValue();
					oCustomer.Name1 = oCore.byId("idCustCompanyName").getValue();
					oCustomer.TaxCenter = oCore.byId("idCustomerTax").getValue();
				}

				var oAdres = {};
				oAdres.StrSuppl1 = oCore.byId("idCustomerAdres").getValue();
				oAdres.StrSuppl3 = oCore.byId("idCustomerAdres2").getValue();
				oAdres.Telephonemob = oCore.byId("idCustMobileNumber").getValue();
				oAdres.Country = oCore.byId("idCustomerCountry").getSelectedKey();
				oAdres.Region = oCore.byId("idCustomerCity").getSelectedKey();
				oAdres.CityNo = oCore.byId("idCustomerDist").getSelectedKey();

				oCustomer.ToAddress = oAdres;

				var oPermission = [];

				oPermission.push({
					Partner: "",
					Zzfld00003iTxt: "Hepsi",
					Zzfld00003i: "HEP",
					Zzfld00003jTxt: "Verilmedi",
					Zzfld00003j: "002",
					Zzfld00003k: "",
					Zzfld00003l: new Date(),
					Zzfld00003m: "",
					Zzfld00003n: ""
				});

				oCustomer.ToPermissionSet = oPermission;

				var that = this;
				var oBusyDialog = new sap.m.BusyDialog();

				oBusyDialog.open();

				oModelCust.create("/customerHeaderSet", oCustomer, {
					success: function(oData, oResponse) {
						// oModelJsonList.setData(oData);
						that.getView().byId("inpu2").setValue(oData.Partner);
						that.getView().byId("inpu201").setValue(oData.FullName);
						// if (oData.FirstName !== "") // Bireysel müşteri
						// {
						// 	that.getView().byId("inpu201").setValue(oCustomer.Firstname + oCustomer.Lastname);
						// } else {
						// 	that.getView().byId("inpu201").setValue(oCustomer.Name1);
						// }
						oBusyDialog.close();
						that.CustomerCreate.close();
					},
					error: function(oError) {
						oBusyDialog.close();
						var message = oError.responseText;
						sap.m.MessageBox.show(message, {
							icon: sap.m.MessageBox.Icon.ERROR,
							title: "HATA",
							actions: [sap.m.MessageBox.Action.OK]
						});
					}
				});

			} else {
				var oMessage = "Lütfen Tüm Zorunlu Alanları Doldurunuz";
				sap.m.MessageBox.show(oMessage, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "UYARI",
					actions: [sap.m.MessageBox.Action.OK]
				});
			}

		},

		custAlanKontrol: function() {
			var oCore = sap.ui.getCore();
			var oVal = true;

			// if (oCore.byId("idGroup").getSelectedKey() === "" && oCore.byId("idGroup").getVisible()) {
			// 	oCore.byId("idGroup").setValueState(sap.ui.core.ValueState.Error);
			// 	oVal = false;
			// } else {
			// 	oCore.byId("idGroup").setValueState(sap.ui.core.ValueState.None);
			// }

			if (oCore.byId("idCustomerRole").getSelectedKey() === "" && oCore.byId("idCustomerRole").getVisible()) {
				oCore.byId("idCustomerRole").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustomerRole").setValueState(sap.ui.core.ValueState.None);
			}

			if (oCore.byId("idCustomerName").getValue() === "" && oCore.byId("idCustomerName").getVisible()) {
				oCore.byId("idCustomerName").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustomerName").setValueState(sap.ui.core.ValueState.None);
			}

			if (oCore.byId("idCustomerSurName").getValue() === "" && oCore.byId("idCustomerSurName").getVisible()) {
				oCore.byId("idCustomerSurName").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustomerSurName").setValueState(sap.ui.core.ValueState.None);
			}

			if (oCore.byId("idCustCompanyName").getValue() === "" && oCore.byId("idCustCompanyName").getVisible()) {
				oCore.byId("idCustCompanyName").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustCompanyName").setValueState(sap.ui.core.ValueState.None);
			}

			if (oCore.byId("idCustMobileNumber").getValue() === "" && oCore.byId("idCustMobileNumber").getVisible()) {
				oCore.byId("idCustMobileNumber").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustMobileNumber").setValueState(sap.ui.core.ValueState.None);
			}

			return oVal;
		},

		onCountryChange: function() {
			var oView = this.getView();
			var oModelCust = this.getView().getModel("CUST");
			var oCore = sap.ui.getCore();
			var oCountry = oCore.byId("idCustomerCountry").getSelectedKey();
			var oFilters = [];
			var oFilter = {};

			oCore.byId("idCustomerCity").setSelectedKey("");
			oCore.byId("idCustomerDist").setSelectedKey("");

			oFilters = [];
			oFilter = new Filter("Land1", FilterOperator.EQ, oCountry);
			oFilters.push(oFilter);

			oModelCust.read("/searchHelpRegionSet", {
				filters: oFilters,
				success: function(oData, response) {
					var oModelJsonList = new sap.ui.model.json.JSONModel(oData);
					oModelJsonList.setData(oData);
					oView.setModel(oModelJsonList, "oSehir");
				}
			});

		},
		onCityChange: function() {
			var oView = this.getView();
			var oModelCust = this.getView().getModel("CUST");
			var oCore = sap.ui.getCore();
			var oCountry = oCore.byId("idCustomerCountry").getSelectedKey();
			var oCity = oCore.byId("idCustomerCity").getSelectedKey();

			var oFilters = [];
			var oFilter = {};

			oCore.byId("idCustomerDist").setSelectedKey("");

			oFilters = [];
			oFilter = new Filter("Country", FilterOperator.EQ, oCountry);
			oFilters.push(oFilter);
			oFilter = new Filter("Region", FilterOperator.EQ, oCity);
			oFilters.push(oFilter);

			oModelCust.read("/searchHelpCitySet", {
				filters: oFilters,
				success: function(oData, response) {
					var oModelJsonList = new sap.ui.model.json.JSONModel(oData);
					oModelJsonList.setData(oData);
					oView.setModel(oModelJsonList, "oIlce");
				}
			});

		},
		onCustomerTypeChange: function() {
			var oCore = sap.ui.getCore();

			var oSelected = oCore.byId("idCustomerType").getSelectedKey();

			if (oSelected === "1") {
				oCore.byId("idCustomerName").setVisible(true);
				oCore.byId("idCustomerSurName").setVisible(true);
				oCore.byId("idCustomerID").setVisible(true);

				oCore.byId("idCustomerTax").setVisible(false);
				oCore.byId("idCustomerTaxNo").setVisible(false);
				oCore.byId("idCustCompanyName").setVisible(false);
			} else if (oSelected === "2") {
				oCore.byId("idCustomerName").setVisible(false);
				oCore.byId("idCustomerSurName").setVisible(false);
				oCore.byId("idCustomerID").setVisible(false);

				oCore.byId("idCustomerTax").setVisible(true);
				oCore.byId("idCustomerTaxNo").setVisible(true);
				oCore.byId("idCustCompanyName").setVisible(true);
			}

		},
		handleProductDetail: function(oEvent) {
			var ProdId = oEvent.getSource().getBindingContext("oModel").getProperty("OrderedProd");
			var aFilter = [];
			if (ProdId) {
				aFilter.push(new Filter("ProductId", FilterOperator.EQ, ProdId));
			}
			if (!this._oDialog) {
				this._oDialog = sap.ui.xmlfragment("zcrm.zcrm_sales_order_operation.fragments.ProdDetail", this);
				this.getView().addDependent(this._oDialog);
				this._oDialog.open();
			} else {

				this._oDialog.open();
			}

			aFilter.push(new Filter("AppNameFilter", FilterOperator.EQ, 'SATIS'));
			var oTable = sap.ui.getCore().byId("ProdDetail");
			var oBinding = oTable.getBinding("items");
			oBinding.filter(aFilter);

		},
		// onProdUpdateFinished: function(evt) {

		// 	var items = evt.getSource().getItems();
		// 	var object;

		// 	for (var i = 2; i < items.length; i++) {

		// 		object = null;

		// 		if (i % 2 == 0) {
		// 			object = items[i].oBindingContexts.PROD.getObject();
		// 			object.ProdImage2 = items[i + 1].oBindingContexts.PROD.getObject().ProdImage;
		// 			delete items[i + 1].oBindingContexts.PROD.getObject().ProdImage;
		// 		}
		// 	}

		// },
		handleProdDetailCancel: function() {

			this._oDialog.close();
		},

		handlePopoverProd: function(oEvent) {
			var Img = oEvent.getSource().getSrc();
			this.getView().getModel("local").setData({
				Img: Img
			});
			if (!this._oPopover) {
				this._oPopover = sap.ui.xmlfragment("zcrm.zcrm_sales_order_operation.fragments.ProdImage", this);
				this.getView().addDependent(this._oPopover);
			}

			var oButton = oEvent.getSource();
			jQuery.sap.delayedCall(0, this, function() {
				this._oPopover.openBy(oButton);
			});
		},
		loadConfigItem: function(oEvent) {
			var oModel = oEvent.getSource().getModel("oConfig");
			var oContextData = oEvent.getSource().getBindingContext("oConfig").getObject();

			this.loadLineConfig(oContextData, oModel);
		},
		loadLineConfig: function(line, updatedModel) {
			var sClass = line.Zzclass;
			var sOrderedProd = line.ZzorderedProd;

			this.BaseValueHelp("/itemConfigCharacteristicSet", "/oKarakteristik", {
				Class: sClass,
				OrderedProd: sOrderedProd
			}, undefined, function(oData) {
				line.__oKarakteristik = oData.results;
				if (updatedModel) {
					updatedModel.updateBindings();
				}
			}.bind(this));
		},
		loadConfigSearchHelp: function(oModel, oLine) {
			var sClass = oLine.Zzclass;
			var sOrderedProd = oLine.ZzorderedProd;

			this.BaseValueHelp("/itemConfigCharacteristicSet", "/oKarakteristik", {
				Class: sClass,
				OrderedProd: sOrderedProd
			}, undefined, function(oData) {
				oLine.__oKarakteristik = oData.results;
				oModel.updateBindings();
			});
		},
		onTahsilatiOnayla: function() {

			var oModel = this.getView().byId("TahsilatTabId").getModel("oModel");
			var oTab = oModel.getProperty("/ToTahsilat/results");
			oTab.forEach(function(item) {
				item.Zzfld0000bp = ((+item.Zzfld00007y || 0) + (+item.Zzfld00007x || 0)) + "";
			});
			oModel.updateBindings();
			this.onSave(false, function(oData) {
				this.recalculateAllFields();
			}.bind(this));
			// this.onSave(false);
			// oTab[oInd].Zzfld0000bp = ((+oTab[oInd].Zzfld00007y || 0) + (+oTab[oInd].Zzfld00007x || 0)) + "";
			// oModel.setProperty("/ToTahsilat/results", oTab);
		},
		onReqDlvDate: function() {
			var oModel = this.getView().getModel("oModel");
			var date = oModel.getProperty("/ToSales/ReqDlvDate");
			var results = oModel.getProperty("/ToItems/results");

			if (results) {
				results.forEach(function(line) {
					line.TeslimatTrh = date;

				});
			}
			oModel.updateBindings();

		},
		onSevkiyatYeri: function(oEvent, oSevkiyat) {
			var oView = this.getView();
			var oAdres = oView.byId("inpu6");
			if (this.oOrderType === "ZS20" || this.oOrderType === "ZA01") {
				if (oEvent) {
					var sevkiyat = oEvent.getSource().getSelectedKey();
				} else {
					sevkiyat = oSevkiyat;
				}
				if (sevkiyat === "0001") {
					var oCode = "0001";
					this.BaseValueHelp("/defaultValuesSet", undefined, {
						Code: oCode
					}, undefined, function onSuccess(oData) {
						this.getView().getModel("oModel").setProperty("/Zzfld000084", oData.results[0].Key);
						var oCode2 = "0003";
						var oExtension = oData.results[0].Key;

						this.BaseValueHelp("/valueHelpSet", undefined, {
							Code: oCode2,
							Extension: oExtension
						}, undefined, function onSuccess(oData) {
							var oModelJsonList3 = new sap.ui.model.json.JSONModel(oData);
							oAdres.setModel(oModelJsonList3, "oAdres");

						}.bind(this));

					}.bind(this));

				} else {
					this.getView().getModel("oModel").setProperty("/Zzfld000084", "");
				}
			}
		},
		onRefid: function(oEvent) {
			var oSource = oEvent.getSource();
			var sItem = oSource.getSelectedKey();
			var oSelectedItem = oSource.getBindingContext("oModel").getObject();
			var oModel = this.getView().getModel("oModel");
			var aTahsilat = oModel.getProperty("/ToTahsilat/results");
			aTahsilat.forEach(function(oRefItem) {
				if (oRefItem.Zzfld00005v === sItem) {
					oRefItem.Zzfld000045 = "02";
					oSelectedItem.Zzfld00007y = oRefItem.Zzfld00007y;
					oSelectedItem.Zzfld0000bp = oRefItem.Zzfld0000bp;
				}
			});
			oModel.updateBindings();
			this.recalculateAllFields();
		},
		onOdemeKosulu: function(oEvent) {
			if (this.oOrderType === "ZA01") {
				var oModel = this.getView().getModel("oModel");
				var sPartner = oModel.getProperty("/ToPartners/results/0/PartnerNo");
				var sDate = oModel.getProperty("/PostingDate");
				var oCode = "0005"; //ödeme tipi
				var oExtension = this.oOrderType;
				this.BaseValueHelp("/valueHelpSet", "/oOdeme", {
					Code: oCode,
					Extension: oExtension,
					Extension2: sPartner,
					Date: sDate
				});
			}
		},
		onChangeOdemeKosulu: function(oEvent) {
			if (this.oOrderType === "ZA01") {
				var oModel = this.getView().getModel("oModel");
				var sPartner = oModel.getProperty("/ToPartners/results/0/PartnerNo");
				var sOdemeKosulu = oEvent.getSource().getSelectedKey();
				var oCode = "0006"; //ödeme koşulu
				var oExtension = this.oOrderType;

				this.BaseValueHelp("/valueHelpSet", "/oOdemeBicim", {
					Code: oCode,
					Extension: oExtension,
					Extension2: sPartner,
					Extension3: sOdemeKosulu
				}, undefined, function onSuccess(oData) {
					oModel.setProperty("/ToPricing/PaymentMethod", oData.results[0].Key);

				}.bind(this));
			}
		},
		onTahsilatBilgisiHesapla: function(oEvent) {
			var oSource = oEvent.getSource();
			var oItem = oSource.getBindingContext("oModel").getObject();
			var sFieldName = oSource.getBindingPath("value");
			var iCurrentValue = oEvent.getParameter("value");
			this.refreshTahsilatCalculation(sFieldName, oItem, iCurrentValue);
			//this.recalculateAllFields();
		},

		refreshTahsilatCalculation: function(sFieldName, oItem, iCurrentValue) {

			var oModel = this.getView().getModel("oModel");
			var oItems = oModel.getProperty("/ToTahsilat/results");
			var iTotal = 0;
			var iTotalAllOdemeTip = 0;
			var iTotalOdenen = 0;
			var oOzelIskonto = oModel.getProperty("/ToCustomerExt/ZzSptiskonto");
			var oBekleyenIskonto = oModel.getProperty("/ToCustomerExt/Zzfld0000ay");
			var oTahsilatInd = oModel.getProperty("/ZzIndirimTutar");
			var oTahsilatIndOncesi = oModel.getProperty("/Zzfld00008d");

			oItems.forEach(function(item) {

				// if (item.Zzfld00009h) continue;

				if (sFieldName === "Zzfld00007x") {
					if (item === oItem) {
						iTotal += +iCurrentValue || 0;
					} else {
						iTotal += +item[sFieldName] || 0;
					}

				} else {
					if (item.Zzfld000044 !== "01" && item.Zzfld000044 !== "02" &&
						item.Zzfld000044 !== "03" && item.Zzfld000044 !== "10" &&
						item.Zzfld000044 !== "12" && item.Zzfld000044 !== "14" &&
						item.Zzfld000044 !== "11" && item.Zzfld000044 !== "") {

						if (item === oItem) {
							iTotal += +iCurrentValue || 0;
						} else {
							iTotal += +item[sFieldName] || 0;
						}
					}
					if (item === oItem) {
						iTotalAllOdemeTip += +iCurrentValue || 0;
					} else {
						iTotalAllOdemeTip += +item[sFieldName] || 0;
					}

				}

				if (item.Zzfld00005w) {
					iTotalOdenen += +item.Zzfld00007y || 0;
				}
			});
			var oFark;
			var oToplamModel = this.getView().getModel("oToplamModel");
			oToplamModel.setProperty("/" + sFieldName, iTotal);
			var oIndirimTutarToplam = oToplamModel.getProperty("/Zzfld00007x") || 0.00;
			var oOdenen = oToplamModel.getProperty("/Zzfld00007y") || 0.00;
			oTahsilatInd = +oOzelIskonto + +oBekleyenIskonto + +oIndirimTutarToplam;
			var oTahsilatIndSonrası = oTahsilatIndOncesi - oTahsilatInd;
			// var oFark = oTahsilatIndSonrası - oTahsilatIndOncesi + +oTahsilatInd;

			oFark = oTahsilatIndSonrası - iTotalAllOdemeTip + iTotalOdenen;
			var KalanBorc = oTahsilatIndSonrası - oOdenen;
			oModel.setProperty("/Zzfld000057", oTahsilatIndSonrası.toFixed(2) + "");
			oModel.setProperty("/ZzIndirimTutar", oTahsilatInd.toFixed(2) + "");
			oModel.setProperty("/ZzOdenenTutar", oOdenen.toFixed(2) + "");
			oModel.setProperty("/ZzKalanTutar", KalanBorc.toFixed(2) + "");
			oModel.setProperty("/Zzfld00008c", oFark.toFixed(2) + "");

		},

		recalculateAllFields: function() {
			["Zzfld00007x", "Zzfld00007y"].forEach(function(sFieldName) {
				this.refreshTahsilatCalculation(sFieldName);
			}.bind(this));
		},
		setEnabledforMakbuz: function(odemeTipi, RecordId, value) {
			if (value !== "" && value !== undefined) {
				return false;
			}

			if (odemeTipi === "04" && (RecordId === "" || RecordId === null || RecordId === undefined)) {
				return false;
			} else {
				return true;
			}
		},

		onMakbuzCikti: function(oEvent) {
			var that = this;
			var sPath = oEvent.getSource().getParent().getBindingContext("oModel").sPath;
			var n = this.getView().getModel("oModel").getProperty(sPath);

			var oBusyDialog = new sap.m.BusyDialog();

			var model = this.getView().getModel();
			var path = "/showPdfMakbuzSet(Hguid=guid'" + n.ParentId + "',Tguid=guid'" + n.RecordId + "')";

			oBusyDialog.open();
			model.read(path, {
				success: function(oData, response) {

					oBusyDialog.close();
					if (response.data.Makbuz !== "" && response.data.Makbuz !== null && response.data.Makbuz !== undefined) {
						var oInd = sPath.slice(-1);
						var oModel = that.getView().byId("TahsilatTabId").getModel("oModel");
						var oTab = oModel.getProperty("/ToTahsilat/results");
						oTab[oInd].Zzfld0000at = response.data.Makbuz;
						oModel.setProperty("/ToTahsilat/results", oTab);

					}
					window.open(response.data.Url);

				},
				error: function(oError) {
					oBusyDialog.close();
				}

			});

		},

		setEnabledforPrint: function(odemeTuru, makbuzNo, kasayaGonder) {
			if (odemeTuru === "04" && kasayaGonder === true) { // && makbuzNo !== "" && makbuzNo !== undefined && makbuzNo !== null
				return true;
			} else {
				return false;
			}

		},

  
		  _setbarcode: function (Barcode) {
			  var that = this;
			var oModelProd = this.getView().getModel("PROD");
			var oTab = this.getView().byId("SepetTabId");
			var List = oTab.getModel("oModel").getProperty("/ToItems/results");
  
			var oFilters = [];
  
			oFilters.push(
			  new Filter({
				path: "ProductId",
				value1: Barcode,
				operator: FilterOperator.EQ,
			  })
			);
  
			BusyIndicator.show();
			var Promise1 = new Promise(function (resolve, reject) {
			  oModelProd.read("/productCatalogSearchSet", {
				filters: oFilters,
				success: function (oData, response) {
				  // this.setOwnerModelProperty("search", "/oCatalog", oData.results);
				  BusyIndicator.hide();
				  try {
					var config = oData.results[0].Config;
				  } catch (error) {
					config = "";
				  }
				  resolve(config);
				}.bind(this),
				error: function (oError) {
				  BusyIndicator.hide();
				},
			  });
			});
  
			Promise1.then(function(result) {
  
			  var oRow = {
				  OrderedProd: Barcode,
				  Description: "",
				  Quantity: "1.000",
				  ProcessQtyUnit: "ADT",
				  Conditionamount1: "0.000",
				  Brmfytkdvli: "0.000",
				  KampInd: "0.000",
				  ThslatInd: "0.000",
				  Zzfld00008x: "0.000",
				  Currency: "",
				  Zzerpstatus: "",
				  Zzspecad: "",
				  Zzteshirdepo: "",
				  ShowConfigButton: result,
				  // Zzfld000083: oKalemNo,
				  // ZzaTeshirblgno: oSipNo,
				  // ZzTeshirind: oIndirim
				};
	  
				List.push(oRow);
	  
				oTab.getModel("oModel").setProperty("/ToItems/results", List);
				oTab.getModel("oModel").refresh();
				that.onSave(false);
			  
			}, function(err) {
			  console.log(err); // Error: 
			});
  
		 
		  }

	});

});