sap.ui.define([
	"./BaseController",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageBox",
	"sap/ui/core/Fragment"
], function (BaseController, Filter, FilterOperator, MessageBox, Fragment) {
	"use strict";

	return BaseController.extend("zcrm.zcrm_sales_order_operation.controller.main", {
		onInit: function () {
			BaseController.prototype.onInit.apply(this, arguments);
			this.getView().addStyleClass("sapUiSizeCompact");
			
				var oFuncType = "001";//belge türü
						this.BaseValueHelp("/processTypeSearchHelpSet", "/BelgeTuru", {
							FuncType : oFuncType
						});

		},
		onAfterRendering: function () {
			// var oFilters = [];
			// var oModel = this.getView().getModel();
			// var oDurum = this.getView().byId("idDurum");

			// var oFilter = new Filter("ProcessType", FilterOperator.EQ, "ZS05");
			// oFilters.push(oFilter);
			// // ekrandaki durum arama yardımı
			// oModel.read("/orderStatusSearchHelpSet", {
			// 	filters: oFilters,
			// 	success: function (oData, response) {
			// 		var oModelJsonList = new sap.ui.model.json.JSONModel(oData);
			// 		oDurum.setModel(oModelJsonList, "oDurum");
			// 	},
			// 	error: function (oError) {}
			// });
		},
		onFilterSearch: function (oEvent) {

			var oKayitSayi = this.getView().byId("idKayitSayisi").getValue();

			var oFilters = [];
			var aSiparisNo = this.getView().byId("inputObjectId").getValue();
			if (aSiparisNo !== "") {
				var oFilter = new Filter("ObjectId", FilterOperator.EQ, aSiparisNo);
				oFilters.push(oFilter);
			}

			var aSiparisVeren = this.getView().byId("inputPartnerNameId").getValue();
			if (aSiparisVeren !== "") {
				oFilter = new Filter("PartnerName", FilterOperator.EQ, aSiparisVeren);
				oFilters.push(oFilter);
			}

			var aSorumluCalisan = this.getView().byId("inputResponsibleNameId").getValue();
			if (aSorumluCalisan !== "") {
				oFilter = new Filter("ResponsibleLastname", FilterOperator.EQ, aSorumluCalisan);
				oFilters.push(oFilter);
			}

			var aBelgeTur = this.getView().byId("idProcessType").getSelectedKey();
			if (aBelgeTur !== "") {
				oFilter = new Filter("ProcessType", FilterOperator.EQ, aBelgeTur);
				oFilters.push(oFilter);
			}

			var aSatisBuro = this.getView().byId("idSBuro").getSelectedKey();
			if (aSatisBuro !== "") {
				oFilter = new Filter("SalesOffice", FilterOperator.EQ, aSatisBuro);
				oFilters.push(oFilter);
			}

			// var oSipTarih = this.getView().byId("idSipTarih");
			// if (oSipTarih.getFrom()) {
			// 	oFilter = new Filter("PostingDate", FilterOperator.BT, oSipTarih.getFrom(), oSipTarih.getTo());
			// 	oFilters.push(oFilter);
			// }

			var SipTarihFrom = this.getOwnerModelProperty("headersearch", "/SipTarih");
			var SipTarihTo = this.getOwnerModelProperty("headersearch", "/SipTarihto");

			if (SipTarihFrom) {
				var utcDateFrom = new Date(Date.UTC(SipTarihFrom.getFullYear(), SipTarihFrom.getMonth(), SipTarihFrom.getDate()));
				var utcDateTo = new Date(Date.UTC(SipTarihTo.getFullYear(), SipTarihTo.getMonth(), SipTarihTo.getDate()));
				oFilter = new Filter({
					path: "PostingDate",
					operator: FilterOperator.BT,
					value1: utcDateFrom,
					value2: utcDateTo
				});
				oFilters.push(oFilter);
			}
			
			var SozTarihFrom = this.getOwnerModelProperty("headersearch", "/SozTarih");
			var SozTarihto = this.getOwnerModelProperty("headersearch", "/SozTarihto");

			if (SozTarihFrom) {
				var utcSozTarihFrom = new Date(Date.UTC(SozTarihFrom.getFullYear(), SozTarihFrom.getMonth(), SozTarihFrom.getDate()));
				var utSozTarihto = new Date(Date.UTC(SozTarihto.getFullYear(), SozTarihto.getMonth(), SozTarihto.getDate()));
				oFilter = new Filter({
					path: "PostingDate",
					operator: FilterOperator.BT,
					value1: utcSozTarihFrom,
					value2: utSozTarihto
				});
				oFilters.push(oFilter);
			}

			// var oSozTarih = this.getView().byId("idSozTarih");
			// if (oSozTarih.getFrom()) {
			// 	oFilter = new Filter("SozlTarihi", FilterOperator.BT, oSozTarih.getFrom(), oSozTarih.getTo());
			// 	oFilters.push(oFilter);
			// }

			var aDurum = this.getView().byId("idDurum").getSelectedKey();
			if (aDurum !== "") {
				oFilter = new Filter("Status", FilterOperator.EQ, aDurum);
				oFilters.push(oFilter);
			}

			var aMaxRow = this.getView().byId("idKayitSayisi").getValue();
			if (aMaxRow !== "") {
				oFilter = new Filter("MaxRows", FilterOperator.EQ, aMaxRow);
				oFilters.push(oFilter);
			}

			var oTable = this.getView().byId("SalesTabId");
			var oModel = oTable.getModel();
			var oModelJsonList = new sap.ui.model.json.JSONModel();

			var oBusyDialog = new sap.m.BusyDialog();
			oBusyDialog.open();

			oModel.read("/salesOrderSet", {
				filters: oFilters,
				success: function (oData, response) {
					 
					oModelJsonList.setData(oData);
					oModelJsonList.setSizeLimit(oKayitSayi);
					oTable.setModel(oModelJsonList, "oSalesModel");
					oBusyDialog.close();
				},
				error: function (oError) {
					oBusyDialog.close();
				}
			});
		},

		onItemPress: function (oEvent) {
			var oNo = oEvent.getSource().getBindingContext("oSalesModel").getObject().Guid;
			sap.ui.core.UIComponent.getRouterFor(this).navTo("detail", {
				guid: oNo
			});
		},
		toCreate: function (oEvent) {
				if (sap.ui.getCore().getModel("oModelGlobal")) {
				sap.ui.getCore().setModel(null, "oModelGlobal");
			}
				var sFunctype = "001";
				
			var oSource = oEvent.getSource();
				this.BaseValueHelp("/processTypeSearchHelpSet", "/ProcessTypeModel", {
					FuncType: sFunctype
				},undefined, function onSuccess(oData) {

			this.open("NewOrderType").setSource(oSource);
						}.bind(this));

			// var oView = this.getView();

			// // Yeni sipariş ekranına geçerken sipariş türünü seçmesi lazım kullanıcının
			// if (!this._OrderType) {
			// 	this._OrderType = sap.ui.xmlfragment("zcrm.zcrm_sales_order_operation.fragments.NewOrderType", this);
			// 	this.getView().addDependent(this._OrderType);
			// 	this._OrderType.open();
			// } else {
			// 	this._OrderType.open();
			// }
		},

		// handleOrderTypeSelect: function () {
		// 	 

		// 	var oText = sap.ui.getCore().byId("idOrderTypes").getSelectedButton().getText();

		// 	var oId = oText.substr(oText.length - 4);
		// 	this._OrderType.close();

		// 	sap.ui.core.UIComponent.getRouterFor(this).navTo("CreateEdit", {
		// 		guid: "00000000-0000-0000-0000-000000000000",
		// 		orderType: oId
		// 	});

		// },
		handleOrderTypeCancel: function () {
			this._OrderType.close();
		},

		onBTurChange: function (oEvent) { // Belge türü seçildiği zaman durum arama yardımı ona göre dolacak.
			var oFilters = [];
			var oKey = oEvent.getSource().getSelectedKey();
			var oModel = this.getView().getModel();
			var oDurum = this.getView().byId("idDurum");

			var oFilter = new Filter("ProcessType", FilterOperator.EQ, oKey);
			oFilters.push(oFilter);

			oModel.read("/orderStatusSearchHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList = new sap.ui.model.json.JSONModel(oData);
					oDurum.setModel(oModelJsonList, "oDurum");
				},
				error: function (oError) {}
			});
		},

	});
});